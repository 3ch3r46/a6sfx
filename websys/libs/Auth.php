<?php
@session_start();
class Auth {
	public $isAdmin=false;
	public $isUser=false;
	public $isGuest=true;
	
	public function setAuth($options=array())
	{
		$_SESSION['attributes']=$options;
		$_SESSION['login']=true;
		foreach($options as $key=>$value)
		{
			$_SESSION[$key]=$value;
		}
		if(isset($_SESSION['login'])){
			$this->isGuest=false;
		}
		if($_SESSION['level']=="admin"){
			$this->isAdmin=true;
		}elseif($_SESSION['level']=="user"){
			$this->isUser=true;
		}
	}
	
	public function unsetAuth()
	{
		@session_start();
		foreach($_SESSION['attributes'] as $key=>$value)
		{
			unset($_SESSION[$key]);
		}
		unset($_SESSION['login']);
		unset($_SESSION['attributes']);
	}
	
    public static function handleUser($direct)
    {
        if($_SESSION['level']!='admin')
        {
            $users=$_SESSION['user'];
            AsHtml::direct($direct);
            return false;
        }
    }
    
    public static function user()
    {
    	return $_SESSION['user'];
    }
    
    public static function handleLogin()
    {
        $login=$_SESSION['login'];
        if($login==false)
        {
            session_destroy();
            AsHtml::direct('login');
            exit;
            return false;
        }
    }
    
    public static function level()
    {
        return $_SESSION['level'];
    }
    public static function adminOnly()
    {
    	if($_SESSION['level']!="admin")
    	{
    		AsHtml::direct('errors');
    	}
    }
    
    public static function getIsAdmin()
    {
    	if($_SESSION['level']=="admin") return true;
    }
    
    public static function getIsUser()
    {
    	if($_SESSION['level']=="user") return true;
    }
    
    public static function getIsLogin()
    {
    	if($_SESSION['login']==true) return true;
    	if($_SESSION['login']==false) return false;
    }
    
    public static function getIsGuest()
    {
    	if($_SESSION['login']==false) return true;
    }
    
    public static function getUserAccess($user)
    {
    	if($user=='*') //allow all user and guest
    	{
    		return true;
    	}
    	elseif($user=='?') //allow for guest and deny for user
    	{
    		return self::getIsGuest();
    	}
    	elseif($user=='@') //allow for all user login
    	{
    		return self::getIsLogin();
    	}
    	elseif($user=='#') //allow for admin only
    	{
    		return self::getIsAdmin();
    	}
    	elseif($user==self::user()) //allow for set username
    	{
    		return true;
    	}
    	else{
    		return false;
    	}
    }
    
    public static function getRolesAccess($role)
    {
    	if(APPS::auth()->isAdmin==true)
    	{
			return true;
    	}else{
    		switch ($role)
    		{
    			case 'isGuest':
    				return self::getIsGuest();
    			case 'isUser':
    				return self::getIsUser();
    				
    			default:
    				return false;
    		}
    	}
    }
}
?>
