<?php
class View
{
	public $widgets=array();
	
	function __construct()
	{
		@define('Powered','<span class=\'powered\'>Powered by 3ch3r46</span>');
		$this->app=new APPS();
	}

	public function render($name,$options=array())
	{
		extract($options);
		$this->setContent($name);
		$this->renderHeader();
		require 'websys/views/'.$name.'.php';
//		$content=include('websys/views/'.$name.'.php');
//		$content=$this->content($name);
//		require 'websys/views/main.php';
		$this->renderFooter();
	}
	
	public function setContent($name)
	{
		$this->content=$name;
	}
	public function content()
	{
		require 'websys/views/'.$this->content.'.php';
	}
	
	public function renderHeader()
	{
		require 'websys/views/header.php';
	}
	
	public function renderFooter()
	{
		require 'websys/views/footer.php';
	}
	
	public function beginWidget($name,$arguments)
	{
		$this->widgets=new $name($arguments);
		return $this->widgets;
	}
	
	public function endWidget()
	{
		$this->widgets->end();
	}
	
	public function registerJScript($options)
	{
		$this->app->registerJScript($options);
	}
	
	public function runJScript()
	{
		$this->app->runJScript();
	}
	
	public function loadJScript($options)
	{
		$this->app->loadJScript($options);
	}
	
	public function loadUrlJScript($options)
	{
		$this->app->loadUrlJScript($options);
	}
	
	public static function registerStyleSheet($options)
	{
		$this->app->registerStyleSheet($options);
	}
	
	public function loadStyleSheet($file)
	{
		$this->app->loadStyleSheet($file);
	}
	
	public function loadUrlStyleSheet($file)
	{
		$this->app->loadUrlStyleSheet($file);
	}
	
	public static function widget($name,$arguments=null,$options=null,$attributes=null)
	{
		return new $name($arguments,$options,$attributes);
	}
	
	public function renderPartial($fileName,$options=array())
	{
		extract($options);
		$dir=$model->model;
		require dirname(__FILE__).'/../views/'.strtolower($dir).'/'.$fileName.'.php';
	}
	
	public function createUrl($url)
	{
		$this->app->createUrl($url);
	}
}