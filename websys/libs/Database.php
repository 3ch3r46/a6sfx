<?php
class Database Extends PDO {
    function __construct($host,$user,$pass)
    {
        parent::__construct($host,$user,$pass);
    }
    
    public function select($opt,$tb,$val=null)
    {
        if(isset($val))
        {
            if(is_array($val))
            {
                $we="WHERE ";
                foreach($val as $key1=>$value1)
                {
                    if(is_numeric($key1))
                    {
                        $we.=$value1.' ';
                    }else{
                    $we.=$key1."=:".$key1.' ';
                    }
                }
            }else{
                $we=$val;
            }
        }
        $sth=$this->prepare("SELECT $opt FROM $tb ".@$we);
        if(isset($val))
        {
            if(is_array($val))
            {
                foreach($val as $key=>$value)
                {
                    if(!is_numeric($key))
                    {
                    $sth->bindValue(':'.$key,$value);
                    }
                }
            }
        }
        $sth->execute();
        return $sth;
    }
    
    public function insert($tb,$data)
    {
        ksort($data);
        
        $fieldName = '`'.implode('`,`',array_keys($data)).'`';
        $fieldValues= ':'.implode(', :',array_keys($data));
        $sth=$this->prepare("INSERT INTO $tb($fieldName) VALUES($fieldValues)");
        foreach($data as $key=>$value)
        {
            $sth->bindValue(':'.$key,$value);
        }
        $sth->execute();
    }
    
    public function update($tb,$data,$where)
    {
        ksort($data);
        $fieldDetails=null;
        foreach($data as $key=>$value)
        {
            $fieldDetails.=$key."=:".$key.",";
        }
        $fieldDetails=rtrim($fieldDetails,',');
        $wheres='';
        if(is_array($where))
        {
            foreach($where as $key=>$value)
            {
                if(is_numeric($key))
                {
                    $wheres.="$value";
                }else{
                $wheres.="$key='$value'";
                }
            }
        }
        $sth=$this->prepare("UPDATE $tb SET $fieldDetails WHERE $wheres");
        foreach($data as $key=>$value)
        {
            $sth->bindValue(':'.$key,$value);
        }
        $sth->execute();
    }
    public function delete($tb,$where)
    {
        foreach($where as $key=>$value)
        {
            if(is_numeric($key))
            {
                $we.=$value.' ';
            }else{
            $we.=$key."=:".$key.' ';
            }
        }
        $sth=$this->prepare("DELETE FROM $tb WHERE $we");
        foreach($where as $key=>$value)
        {
            if(!is_numeric($key))
            {
                $sth->bindValue(':'.$key,$value);
            }
        $sth->execute();
        return $sth;
        }
    }
    public function pagingLimit($pPage,$pNum)
    {
        $offset = ($pNum - 1) * $pPage;
	$q = "limit $offset,$pPage";
        return $q;
    }
    public function pagingPage($ro,$pPage)
    {
        $j = ceil($ro / $pPage);
        return $j;
    }
    public function pagingNav($url,$page,$pRow)
    {
        $pag='<div class="pagging"><div class="btn-group">';
        if($page > 1)
		{
			$pge=$page-1;
			$pag.= "<a href='".APPS::createUrl(array($url,'id'=>1))."' class='btn'>&laquo;</a>";
			$pag.= "<a href='".APPS::createUrl(array($url,'id'=>$pge))."' class='btn'>&lsaquo;</a>";
		}
	for($pg=1;$pg<=$pRow;$pg++)
	{
		if($pg==$page)
		{
			$pag.="<span class='btn btn-primary'>".$pg."</span>";
		}else{
			$pag.="<a href='".APPS::createUrl(array($url,'id'=>$pg))."' class='btn'>".$pg."</a>";
		}
	}
	if($page < $pRow)
	{
		$pge=$page+1;
		$pag.="<a href='".APPS::createUrl(array($url,'id'=>$pge))."' class='btn'>&rsaquo;</a>";
		$pag.="<a href='".APPS::createUrl(array($url,'id'=>$pRow))."' class='btn'>&raquo;</a>";
	}
	$pag.='</div></div>';
	if($pRow==1)
	{
		$pag='';
	}
        return $pag;
    }
}
?>
