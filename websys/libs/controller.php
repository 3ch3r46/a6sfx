<?php
class Controller{
	function __construct(){
		$this->view = new View();
		$this->app=new APPS();
		$this->getAccessRole($action=null);
	}
	
	public function direct($url)
	{
		$this->app->direct($url);
	}
	
	public function getAccessRole($action)
	{
		$data=$this->accessRoles();
		if(isset($data))
		{
			foreach($data as $role)
			{
				foreach($role['action'] as $dataAction)
				{
					if($dataAction==$action){
						if(isset($role['user']))
						{
							if(Auth::getUserAccess($role['user'])==true)
							{
								return true;
							}else{
								return false;
							}
						}elseif(isset($role['roles']))
						{
							foreach($role['roles'] as $roles)
							{
								if(Auth::getRolesAccess($roles)==true)
								{
									return true;
								}else{
									return false;
								}
							}
						}
					}
				}
			}
		}else{
			return false;
		}
	}
	
}