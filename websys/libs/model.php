<?php
class Model
{

	public $model;
	public $table;
	public $pag;
	
	public static $_model=array();
	
	public $attributes=array();
	public $isNewRecord=false;
	
	public $type='allData';
	public $id;
	public $offset=10;
	public $pk='id';
	public $val_pk;
	public $order_by='id';
	public $order_type='desc';
	public $criteria;
	public $val_criteria;
	public $url;
	public $data=array();
	public $dataKey=array();
	public $search=array();
	
	public $errorSummary;
	
	/**
	 * run this class
	 * @return Database Object
	 */
	public function __construct(){
		@$this->db= new Database('mysql:host='.HOSTNAME.';dbname='.DB_NAME,HOST_USERNAME,HOST_PASSWORD);
	}
	
	/**
	 * running model to static mode
	 * @return Model $className
	 * @param class $className
	 */
	public static function model($className=__CLASS__)
	{
		if(isset(self::$_model[$className])){	
			return self::$_model[$className];
			$this->isNewRecord=false;
		}else
		{
			$model=self::$_model[$className]=new $className(null);
			$model->isNewRecord=false;
			return $model;
		}
	}
	
	public function criteriaAttributes($field,$value,$activate=true)
	{
		if($activate==true){
			$this->search[]="'".$field."' LIKE '%".$value."%'";
		}
		return $this;
	}
	
	/**
	 * set new the attributes
	 * @param array $attributes
	 * @return string set the attributes model
	 */
	public function setAttributes(array $attributes=array())
	{
		unset($this->attributes);
		foreach($attributes as $key=>$value)
		{
			if(isset($this->{$key}))
			{
				if($value!==$this->{$key} and $value!=null) $this->attributes[$key]=$value;
			}else{
				$this->attributes[$key]=$value;
			}
		}
		if(isset($this->attributes))
		{
			foreach($this->attributes as $key=>$value)
			{
				$this->{$key}=$value;
			}
		}
	}
	
	public function unsetAttributes()
	{
		foreach($this->attributes as $key=>$value)
		{
			//unset($this->{$key});
		}
	}
	
	/**
	 * To know this model is new record or no
	 * @return boolean isNewRecord
	 */
	public function setNewRecord()
	{
		$this->isNewRecord=true;
	}
	
	/**
	 * Reading record from database
	 * @param array $options
	 * @return multitype: $data, $dat
	 */
	public function readData($options=array())
	{
		foreach($options as $key=>$value)
		{
			$this->{$key}=$value;
		}
		
		switch($this->type){
			case 'single':
				$data=$this->db->select('*',$this->table,array($this->pk=>$this->{$this->pk}));
				$datas=$data->fetch();
				$this->setAttributes($datas);
				break;

			case 'allData':
				$lim=$this->db->pagingLimit($this->offset,$this->{$this->pk});
				$dat=$this->db->select('*',$this->table,'order by '.$this->order_by.' '.$this->order_type.' '.$lim);
				$das=$this->db->select('*',$this->table);
				$ro=$das->rowCount();
				$this->pag=$this->db->pagingPage($ro,$this->offset);
				$data=$dat->fetchAll();
				return $data;
				break;
				
			case 'search':
				$lim=$this->db->pagingLimit($this->offset,$this->{$this->pk});
				$dat=$this->db->select('*',$this->table,"where $this->criteria like '%$this->val_criteria%' order by $this->order_by $this->order_type ".$lim);
				$das=$this->db->select('*',$this->table,"where $this->criteria like '%$this->val_criteria%'");
				$ro=$das->rowCount();
				$this->pag=$this->db->pagingPage($ro,$this->offset);
				return $dat->fetchAll();
				break;
		}
	}

	/**
	 * Get a navigation page
	 * @param array $options
	 */
	public function navigation($options=array())
	{
		foreach($options as $key=>$value)
		{
			$this->{$key}=$value;
		}
		
		return $this->db->pagingNav($this->url,$this->val_pk,$this->pag);
	}

	/**
	 * Save new data or Update data
	 * @param array $options
	 */
	public function saveData($options=array())
	{
		foreach($options as $key=>$value)
		{
			$this->{$key}=$value;
		}
		$data=array();
		foreach($this->attributes as $key=>$value)
		{
			$data[$key]=$this->{$key};
		}
		switch($this->type)
		{
			case 'create':
				$this->db->insert($this->table,$data);
				$this->{$this->pk}=$this->db->lastInsertId($this->{$this->pk});
				break;
			case 'update':
				$dba=$this->db->update($this->table,$data,array($this->pk=>$this->{$this->pk}));
				break;
		}
	}
	
	/**
	 * update record by primary key
	 * @param integer $pk
	 * @param array $data
	 * @return action update data
	 */
	public function updateByPk($pk,$data=array())
	{
		return $this->db->update($this->table,$data,array($this->pk=>$pk));
	}
	
	/**
	 * update record by another field
	 * @param array $field
	 * @param array $data
	 * @return action update data
	 */
	public function updateByField($field=array(),$data=array())
	{
		return $this->db->update($this->table,$data,$field);
	}
	
	/**
	 * Delete record by primary key
	 * @param integer $pk
	 * @return action delete record
	 */
	public function deleteByPk($pk)
	{
		return $this->db->delete($this->table,array($this->pk=>$pk));
	}
	
	/**
	 * Delete data by another field
	 * @property $field is a delete clause
	 * @param array $field
	 * @return PDOStatement
	 */
	public function deleteByField($field=array())
	{
		return $this->db->delete($this->table,$field);
	}
	
	public function getLastRecord()
	{
		$data=$this->db->select('*',$this->table,array($this->pk=>$this->{$this->pk}));
		$datas=$data->fetch();
		$this->setAttributes($datas);
	}
	
	/**
	 * delete data
	 * @param array $options
	 */
	public function deleteData($options=array())
	{
		foreach($options as $key=>$value)
		{
			$this->{$key}=$value;
		}
		
		return $this->db->delete($this->table,array($this->pk=>$this->{$this->pk}));
	}
	
	/**
	 * management data
	 * @return the record in management page
	 */
	public function mData()
	{
		$id=$this->{$this->pk};
		if(@$_GET['search']!=null){
			$list = $this->readData(array(
					'type'=>'search',
					'criteria'=>@$_GET['cry'],
					'val_criteria'=>@$_GET['search'],
					'pk'=>$this->pk,
					'val_pk'=>$id,
					'offset'=>$this->offset,
					'order_by'=>isset($_GET['sort'])?$_GET['sort']:$this->pk,
					'order_type'=>isset($_GET['t'])?$_GET['t']:'desc'
			));
		}elseif(@$_GET['sort']!=null){
			$list = $this->readData(array(
					'type'=>'allData',
					'order_by'=>@$_GET['sort'],
					'order_type'=>@$_GET['t'],
					'val_pk'=>$id,
					'offset'=>$this->offset,
			));
		}else{
			$list = $this->readData(array(
					'type'=>'allData',
					'val_pk'=>$id,
					'offset'=>$this->offset,
					'order_by'=>$this->pk,
					'order_type'=>'desc'
			));
		}
		return $list;
	}
	
	/**
	 * Single get record from database
	 * @return record
	 */
	public function single(){ $this->type='single'; $this->readData(); return $this; }
	
	/**
	 * allData get all record from database
	 * @return multiple record
	 */
	public function allData(){ $this->type='allData'; return $this; }
	
	public function setPk($pk){ $this->pk=$pk; return $this; }
	
	public function findByPk($pk){ $this->{$this->pk}=$pk; $this->single(); return $this; }
	
	public function offset($off){ $this->offset=$off; return $this; }
	
	/**
	 * Save record new or update record
	 * @param boolean $activeValidation
	 */
	public function save($activeValidation=true){
		if($activeValidation==true){
			if($this->validation()){
				$this->beforeSave();
				$this->saveData(array('type'=>$this->isNewRecord?'create':'update'));
				$this->afterSave();
				return true;
			}else{
				return false;
			}
		}else{
			$this->beforeSave();
			$this->saveData(array('type'=>$this->isNewRecord?'create':'update'));
			$this->afterSave();
			return true;
		}
	}
	
	protected function beforeSave(){}
	
	protected function afterSave(){}
	
	public function setData($data){ $this->data=$data; return $this;}
	
	public function setUrl($url){ $this->url=$url; return $this;}
	
	public function orderBy($pk){ $this->order_by=$pk; return $this;}
	
	public function orderType($t){ $this->order_type=$t; return $this; }
	
	public function criteria($c,$v){ $this->criteria=$c; $this->val_criteria=$v; return $this; }
	
	public function delete(){ $this->deleteData(); return $this;}
	
	/**
	 * Error Summary
	 * @return error input in form
	 */
	public function errorSummary()
	{
		if(isset($this->errorSummary)){
			echo "<div class='alert alert-block alert-error'><p>Please fix the following input errors:</p><ul>";
			foreach($this->errorSummary as $value)
			{
				echo "<li><strong>".$value['field']."</strong> ".$value['error'];
			}
			echo "</ul></div>";
		}
	}
	
	/**
	 * add error
	 * @property to add error in error summary
	 * @param string $field
	 * @param string $error
	 */
	public function addError($field,$error)
	{
		$this->errorSummary[]=array('field'=>$field,'error'=>$error);
	}
	
	/**
	 * Checking rules validation
	 * @return boolean
	 */
	public function validation()
	{
		$error=$this->rules();
		foreach($error as $value)
		{
			if(isset($value['message'])) $message=$value['message'];
			if($value[1]=='required') $this->errorRequired($value[0],$message);
			if($value[1]=='length'){
				if(isset($value['messageMin'])) $message['min']=$value['messageMin'];
				if(isset($value['messageMax'])) $message['max']=$value['messageMax'];
				if(isset($value['max'])) $argument['max']=$value['max'];
				if(isset($value['min'])) $argument['min']=$value['min'];
				$this->errorLength($value[0],$argument,$message);
			}
			if($value[1]=='digit') $this->errorDigit($value[0],$message);
			if($value[1]=='alphanumeric') $this->errorAlphaNumeric($value[0],$message);
			if($value[1]=='alphabetic') $this->errorAlpha($value[0],$message);
			if($value[1]=='lowercase') $this->errorLower($value[0],$message);
			if($value[1]=='uppercase') $this->errorUpper($value[0],$message);
			if($value[1]=='numeric') $this->errorNumber($value[0],$message);
			if($value[1]=='email') $this->errorEmail($value[0],$message);
			if($value[1]=='link') $this->errorLink($value[0],$message);
			unset($message);
		}
		if(empty($this->errorSummary)){
			return true;
		}else{
			return false;
		}
	}
	
	private function errorRequired($value,$message=null)
	{
		$fieldData=$this->field();
		$field=explode(',', $value);
		$messages='Field cannot be blank.';
		if(isset($message)) $messages=$message;
		foreach($field as $valus)
		{
			if(empty($this->{$valus})) $this->addError($fieldData[$valus],$messages);
		}
		return $this;
	}
	private function errorLength($value,$options=array(),$message=null)
	{
		$fieldData=$this->field();
		if(is_array($value)){
			$field=explode(',',$value);
		}else{
			$field=$value;
		}
		if(is_array($field))
		{
			foreach($field as $valus)
			{
				if(!empty($this->{$valus})){
					if($options['max']!=''){
						if(strlen($this->{$valus})>$options['max']){
							$error='is too long (maximum is '.$options['max'].' characters).';
							if(isset($message['max'])) $error=$message['max'];
							$this->addError($fieldData[$valus], $error);
						}
					}
					if($options['min']!=''){
						if(strlen($this->{$valus})<$options['min']){
							$error='is too long (minimum is '.$options['min'].' characters).';
							if(isset($message['min'])) $error=$message['min'];
							$this->addError($fieldData[$valus], $error);
						}
					}
				}
			}
		}else{
			if(!empty($this->{$field})){
				if($options['max']!=''){
					if(strlen($this->{$field})>$options['max']){
						$error='is too long (maximum is '.$options['max'].' characters).';
						if(isset($message['max'])) $error=$message['max'];
						$this->addError($fieldData[$field], $error);
					}
				}
				if($options['min']!=''){
					if(strlen($this->{$field})<$options['min']){
						$error='is too long (minimum is '.$options['min'].' characters).';
						if(isset($message['min'])) $error=$message['min'];
						$this->addError($fieldData[$field], $error);
					}
				}
			}
		}
	}
	
	private function errorDigit($value,$message=null)
	{
		$fieldData=$this->field();
		$field=explode(',', $value);
		$messages=" Field must be a digit.";
		if(isset($message)) $messages=$message;
		foreach($field as $valus)
		{
			if(!empty($this->{$valus})){
				if(!ctype_digit($this->{$valus})){
					$this->addError($fieldData[$valus], $messages);
				}
			}
		}
	}
	
	private function errorAlphaNumeric($value,$message=null)
	{
		$fieldData=$this->field();
		$field=explode(',', $value);
		$messages=" Field must be an Alphanumeric  characters.";
		if(isset($message)) $messages=$message;
		foreach($field as $valus)
		{
			if(!empty($this->{$valus})){
				if(!ctype_alnum($this->{$valus})){
					$this->addError($fieldData[$valus], $messages);
				}
			}
		}
	}
	
	private function errorAlpha($value,$message=null)
	{
		$fieldData=$this->field();
		$field=explode(',', $value);
		$messages=" Field must be an Alphabetic characters.";
		if(isset($message)) $messages=$message;
		foreach($field as $valus)
		{
			if(!empty($this->{$valus})){
				if(!ctype_alpha($this->{$valus})){
					$this->addError($fieldData[$valus], $messages);
				}
			}
		}
	}
	
	private function errorLower($value,$message=null)
	{
		$fieldData=$this->field();
		$field=explode(',', $value);
		$messages=" Field must be a lowercase.";
		if(isset($message)) $messages=$message;
		foreach($field as $valus)
		{
			if(!empty($this->{$valus})){
				if(!ctype_lower($this->{$valus})){
					$this->addError($fieldData[$valus], $messages);
				}
			}
		}
	}
	
	private function errorUpper($value,$message=null)
	{
		$fieldData=$this->field();
		$field=explode(',', $value);
		$messages=" Field must be a uppercase.";
		if(isset($message)) $messages=$message;
		foreach($field as $valus)
		{
			if(!empty($this->{$valus})){
				if(!ctype_upper($this->{$valus})){
					$this->addError($fieldData[$valus], $messages);
				}
			}
		}
	}
	
	private function errorNumber($value,$message=null)
	{
		$fieldData=$this->field();
		$field=explode(',', $value);
		$messages=" Field must be a numeric.";
		if(isset($message)) $messages=$message;
		foreach($field as $valus)
		{
			if(!empty($this->{$valus})){
				if(!ctype_xdigit($this->{$valus})){
					$this->addError($fieldData[$valus], $messages);
				}
			}
		}
	}
	
	private function errorEmail($value,$message=null)
	{
		$fieldData=$this->field();
		$field=explode(',', $value);
		$messages=" Field must be an E-mail Address.";
		if(isset($message)) $messages=$message;
		foreach($field as $valus)
		{
			if(!empty($this->{$valus})){
				if(!filter_var($this->{$valus},FILTER_VALIDATE_EMAIL)){
					$this->addError($fieldData[$valus], $messages);
				}
			}
		}
	}
	
	private function errorLink($value,$message=null)
	{
		$fieldData=$this->field();
		$field=explode(',', $value);
		$messages=" Field must be a Link Address.";
		if(isset($message)) $messages=$message;
		foreach($field as $valus)
		{
			if(!empty($this->{$valus})){
				if(!filter_var($this->{$valus},FILTER_VALIDATE_URL)){
					$this->addError($fieldData[$valus], $messages);
				}
			}
		}
	}
}