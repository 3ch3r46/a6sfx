<?php
class Boot
{
	function __construct(){
		$url = isset($_GET['url'])?$_GET['url']:"site";
		$url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
		$url = explode('/',  $url);
		$action=$url[1]!==null?$url[1]:"index";
		$urls=$_SERVER['REQUEST_URI'];
		$urls=ltrim($urls,ROOT);
		$urls=explode('/', $urls);
		$request=explode('?', $urls[2]);
		$dataRequest=explode('&', $request[1]);
		$data=array();
		foreach($dataRequest as $val)
		{
			list($key,$value)=split('=',$val);
			$data[$key]=$value;
		}
		if($url[0]==null) $url[0]='site';
		
		$file = 'websys/controllers/'.ucfirst($url[0]).'Controller.php';
		if(file_exists($file)){
			require $file;
		}else{
            throw new AsErrorException(404, 'Page not found.');
            return false;
		}
        $url[0]=strtolower($url[0]);
        $url[0]=ucfirst($url[0]);
		$cont=$url[0].'Controller';
		$controller = new $cont;
		$urlActions='action'.ucfirst($action);
        //$controller->loadModel($url[0]);
		if(isset($url[2])){
            if(method_exists($controller, $urlActions)){
            	if($controller->getAccessRole($action)==true)
            	{
					$controller->{$urlActions}($url[2]);
            	}
            	else
            	{
            		throw new AsErrorException(403,"Page ".$url[1]." is forbidden to use action in ".$url[0]);
            	}
            }else{
                throw new AsErrorException(404,"Page ".$url[1]." is not exists in content ".$url[0]);
             //   return false;
            }
		}else{
            if(isset($url[1])){
                if(method_exists($controller, $urlActions)){
                	if($controller->getAccessRole($action)==true)
                	{        
                		$controller->{$urlActions}();
                	}
                	else
                	{
                		throw new AsErrorException(403,"Page ".$url[1]." is forbidden to use action in ".$url[0]);
                	}
                }else{
                    throw new AsErrorException(404,"Page <strong>".$url[1]."</strong> is not exists in content ".$url[0]);
               // 	return false;
				}
			}else{
				if($controller->getAccessRole($action)==true)
				{
                	$controller->actionIndex();
                }
                else
                {
                	throw new AsErrorException(403,"Page ".$action." is forbidden to use action in ".$url[0]);
                }
			}
		}
	}
}
?>
