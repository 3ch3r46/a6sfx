<?php
error_reporting(0);
class AsErrorException extends ErrorException
{
	public $errorType;
	
	/**
	 * Error Exception to show error in website
	 * @param string $errorType
	 * @param string $errorMessage
	 */
	
	public function __construct($errorType,$errorMessage)
	{
		parent::__construct($errorMessage);
		$this->errorType=$errorType;
		$this->setErrorException();
	}
	
	public function setErrorException()
	{
		$view=new View();
		$view->renderHeader();
		echo "<div class='alert alert-error'>";
		echo "<h1>Error ".$this->errorType."</h1>";
		echo "<strong>".$this->getMessage()."</strong>";
		echo "</div>";
		$view->renderFooter();
//		require $roots.'footer.php';
	}
}