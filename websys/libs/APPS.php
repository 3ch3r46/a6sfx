<?php
class APPS
{
	public static $_js;
	public static $script;
	public static $fileJS=array();
	public static $style;
	public static $_auth;
	
	public function __construct()
	{
		self::$_auth=new Auth();
	}
	
	public static function widget($name,$arguments=null,$options=null,$attributes=null)
	{
		return new $name($arguments,$options,$attributes);
	}
	
	public static function registerJScript($options)
	{
		self::$script.=$options;
	}
	
	public static function loadJScript($file)
	{
		if(!isset(self::$fileJS[$file]))
			self::$fileJS[$file]=ROOT."public/js/".$file;
	}
	
	public static function loadUrlJScript($file)
	{
		if(!isset(self::$fileJS[$file]))
			self::$fileJS[$file]=$file;
	}
	
	public static function registerStyleSheet($options)
	{
		echo "<style>".PHP_EOL;
		echo $options;
		echo "</style>".PHP_EOL;
	}
	
	public static function loadStyleSheet($file)
	{
		echo "<link rel='stylesheet' href='".ROOT."public/css/".$file."' type='text/css' />".PHP_EOL;
	}
	
	public static function loadUrlStyleSheet($file)
	{
		echo "<link rel='stylesheet' href='".$file."' type='text/css' />".PHP_EOL;
	}
	
	public static function runJScript()
	{
		foreach(self::$fileJS as $value)
		{
			echo "<script src='".$value."' type='text/javascript'></script>".PHP_EOL;
		}
		echo "<script type='text/javascript'>".PHP_EOL;
		echo "jQuery(document).ready(function(){".PHP_EOL;
		echo self::$script;
		echo "});".PHP_EOL;
		echo "</script>";
	}
	
	public static function renderPartial($fileName,$model)
	{
		$dir=$model->model;
		require dirname(__FILE__).'/../views/'.strtolower($dir).'/'.$fileName.'.php';
	}
	
	public static function createUrl($url=array())
	{
		$urls=ROOT;
		foreach($url as $key=>$value)
		{
			if(!is_numeric($key) and $key=="id"){$urls.="/".$value;}
			else{$urls.=$value;}
		}
		return $urls;
	}
	
	public static function direct($url)
	{
		if(is_array($url))
		{
			$urls=null;
			foreach($url as $key=>$value)
			{
				if(!is_numeric($key) and $key=="id"){
					$urls.="/".$value;
				}else{
					$urls.=$value;
				}
			}
			header('location:'.ROOT.$urls);
		}else{
			header('location:'.$url);
		}
	}

	public static function auth()
	{
		return self::$_auth;
	}
}