<?php
class AsModal
{
	public $_id;
	
	public function __construct($options)
	{
		foreach($options as $key=>$val)
		{
			$this->{$key}($val);
		}
		
		$this->beginModal();
	}
	
	public function id($options)
	{
		$this->_id=$options;
	}
	
	public function beginModal()
	{
		echo "<div id='".$this->_id."' class='modal hide fade'>";
	}
	
	public function end()
	{
		echo "</div>";
		APPS::loadJScript('modal.js');
		APPS::registerJScript("
		$(document).ready(function(){
		// Console \"polyfill\"
			if (!console) {
				console = {};
				console.log = function() {};
			}
			$('#$this->_id').modal({'show':false});
				$('#$this->_id').on('show', function() {
					console.log('Modal show.');
				});
				$('#$this->_id').on('shown', function() {
					console.log('Modal shown.');
				});
				$('#$this->_id').on('hide', function() {
					console.log('Modal hide.');
				});
				$('#$this->_id').on('hidden', function() {
					console.log('Modal hidden.');
				});
		});
		");
	}
}
?>