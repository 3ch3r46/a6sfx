<?php
class AsCarousel
{
	public $_id='carousel';
	public $_content;
	public $_imgUrl;
	public $_style;
	
	public function __construct($options)
	{
		$this->app=new View();
		foreach($options as $key=>$val)
		{
			$this->{$key}($val);
		}
		$this->beginCarousel();
		$this->end();
	}
	
	public function id($options)
	{
		$this->_id=$options;
	}
	
	public function style($options)
	{
		$this->_style="style='".$options."'";
	}
	
	public function location($options)
	{
		if(isset($options)) $url=$options;
		$this->_imgUrl=ROOT."/public/images/".$url;
	}
	
	public function beginCarousel()
	{
		echo "<div id='$this->_id' class='carousel slide' $this->_style>
		<div class='carousel-inner'>";
		foreach($this->_content as $key=>$value)
		{
		echo "<div class='item".($key==0?" active":'')."'>
<img src='".$this->_imgUrl.$value['images']."' alt=''>
<div class='carousel-caption'>
<h4>".$value['label']."</h4>
<p>".$value['content']."</p>
</div>
</div>";
		}
	}
	
	public function items($options)
	{
		$this->_content=$options;
	}
	
	public function end()
	{
		echo "</div>";
		echo "<a class='carousel-control left' href='#$this->_id' data-slide='prev'>&lsaquo;</a><a class='carousel-control right' href='#$this->_id' data-slide='next'>&rsaquo;</a>";
		echo "</div>";
		APPS::loadJScript('carousel.js');
		APPS::registerJScript("
			jQuery('#$this->_id').carousel();
			jQuery('#$this->_id').on('slide', function() { console.log('Carousel slide.'); });
			jQuery('#$this->_id').on('slid', function() { console.log('Carousel slid.'); });
		");
	}
}
?>