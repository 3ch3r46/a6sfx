<?php
class AsCustomDetailView
{
	public function __construct($model,$fileDetailView)
	{
		$this->model=$model;
		$this->dataProvider=$model->read();
		extract($this->dataProvider);
		require 'websys/views/'.strtolower($model->model).'/'.$fileDetailView.'.php';
	}

}