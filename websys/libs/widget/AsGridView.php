<?php
require dirname(__FILE__).'/AsTable.php';

class AsGridView extends AsTable
{
	public $header=array();
	public $field=array();
	public $data=array();
	public $model;
	public $useNav;
	public $type='table-condensed';
	public $buttonHeader;
	public $buttonField;
	public $useButton=false;
	public $enableSort=true;
	public $pk;
	public $id_grid='AsGridView';
	public $htmlOptions;
	public $deleteMessage='Are you sure want to delete?';

	/*
	 * @params $options as array type
	 */
	public function __construct($model,$options=array()){
		//AsHtml::style('table');
		$this->data=$model->mData();
		$this->model=$model;
		$this->pk=$model->pk;
		$this->htmlOptions['class']='items table';
		$this->htmlOptions['cellspacing']=0;
		$this->htmlOptions['border']=0;
		$this->htmlOptions['width']='100%';
		foreach($options as $key=>$value)
		{
			$this->{$key}($value);
		}
		$this->htmlOptions['class'].=" ".$this->type;
		$this->htmlOptions['id']=$this->id_grid;
		$this->search();
		echo "<div class='scroll'>".PHP_EOL;
		$this->startTable($this->htmlOptions);
		$this->headers();
		$this->dataField();
		$this->endTable();
		AsHtml::endDiv();
		$this->navigat();
		APPS::loadJScript('google.min.js');
		APPS::loadJScript('jscrollpane.js');
		APPS::registerJScript("
    		$('.del').click(function(c){
    		var c=confirm('".$this->deleteMessage."');
    		if(c == false) return false;
    		});");
		return $this;
	}
	
	public function id($opt)
	{
		$this->id_grid=$opt;
	}
	
	public function deleteMessage($options)
	{
		$this->deleteMessage=$options;
	}
	public function types($opt)
	{
		$options=explode(' ', $opt);
		$optd='';
		foreach($options as $val)
		{
			$optd.="table-".$val." ";
		}
		$this->type=$optd;
	}
	
	public function columns($options=array())
	{
		foreach($options as $key=>$value)
		{
			if(is_array($value))
			{
				foreach($value as $k=>$val)
				{
						if($k=="header") $this->header[]=$val;
						elseif($k=="value") $this->field[]=$val;
						elseif($k=="type") $this->fieldType[$key]=$val;
						elseif($k=="customValue") $this->fieldCustom[$key]=$val;
						else $this->field[]=$value;
				}
			}else{
				$this->header[]=$value;
				$this->field[]=$value;
			}
		}
		return $this;
	}
	
	public function enableSort($options)
	{
		$this->enableSort=$options;
	}
	
	public function buttonAction($options)
	{
		if(is_array($options))
		{
			$this->useButton=true;
			$counter=0;
			$showw='';
			foreach($options as $key=>$value)
			{
					if($value=='detail') $showw.="detail/";
					if($value=='edit') $showw.="edit/";
					if($value=='delete') $showw.="delete/";
					$counter++;
			}
			$showw=rtrim($showw,'/');
			$this->buttonField=$showw;
			if($counter==3) $this->buttonHeader="<span class='spans3'></span>";
			if($counter==2) $this->buttonHeader="<span class='spans2'></span>";
			if($counter==1) $this->buttonHeader="<span class='spans1'></span>";
		}else{
			$this->useButton=$options;
			$this->buttonHeader="<span class='spans3'></span>";
			$this->buttonField='all';
		}
	}
	
	public function buttonField($i)
	{
		$show='';
		if($this->buttonField=='all')
		{
			$show.="<a href='".APPS::createUrl(array($this->model->model.'/detail','id'=>$i))."' class='btn-link'><i class='icon-search'></i></a>";
			$show.="<a href='".APPS::createUrl(array($this->model->model.'/update','id'=>$i))."' class='btn-link'><i class='icon-pencil'></i></a>";
			$show.="<a href='".APPS::createUrl(array($this->model->model.'/delete','id'=>$i))."' class='btn-link del'><i class='icon-remove'></i></a>";
		}else{
			$button=explode('/', $this->buttonField);
			foreach($button as $val)
			{
				if($val=="detail") $show.="<a href='".APPS::createUrl(array($this->model->model.'/detail','id'=>$i))."' class='btn-link'><i class='icon-search'></i></a>";
				if($val=="update") $show.="<a href='".APPS::createUrl(array($this->model->model.'/update','id'=>$i))."' class='btn-link'><i class='icon-pencil'></i></a>";
				if($val=="delete") $show.="<a href='".APPS::createUrl(array($this->model->model.'/delete','id'=>$i))."' class='btn-link del'><i class='icon-remove'></i></a>";
			}
		}
		return $show;
	}
	
	public function headers()
	{
		if(@$_GET['t']=="asc"){
			$sr="&t=desc";
		}elseif((@$_GET['t']=="desc") or !isset($_GET['t'])){
			$sr="&t=asc";
		}
		if(isset($_GET['search'])) $srs="search=".$_GET['search']."&cry=".$_GET['cry']."&";
		$header=array();
		foreach($this->header as $k=>$a)
		{
			if($this->enableSort==true)
				$header[]=AsHtml::link("?".@$srs."sort=".$a.@$sr."#".$this->id_grid,ucfirst(str_replace('_',' ',$a)));
			else
				$header[]=ucfirst(str_replace('_',' ',$a));
		}
		if($this->useButton==true)
		{
			$header[]=$this->buttonHeader;
		}
		$this->headTable($header);
		return $this;
	}
	public function dataField()
	{
		$row=1;
		foreach($this->data as $data)
		{
			$show=array();
			$attrib=array();
			if($row%2==0)
			{
				$attrib['class']='even';
			}else{
				$attrib['class']='odd';
			}
			foreach($this->field as $k=>$b)
			{
				if($this->fieldType[$k]=="raw")
				{
					$show[]=$this->fieldCustom[$k];
				}else{
					$show[]=$data[$b];
				}
			}
			if($this->useButton==true)
			{
				$show[]=$this->buttonField($data[$this->pk]);
			}
			$this->fieldTable($show,$attrib);
			$row++;
		}
		return $this;
	}
	
	public function navigation($options)
	{
		$this->useNav=true;
		foreach($options as $key=>$value)
		{
			$this->{$key}=$value;
		}
	}
	
	public function navigat()
	{
		if($this->useNav==true)
		{
		echo $this->model->navigation(array(
				'url'=>$this->model->model.'/'.$this->urlAction,
				'pk'=>$this->model->{$this->model->pk},
			));
		}
	}
	
	public function searching($options)
	{
		$this->useSearch=true;
		foreach($options as $key=>$value)
		{
			$this->{$key}=$value;
		}
	}
	
	public function search()
	{
		if($this->useSearch==true)
		{
			$fr=new AsForm(array('method'=>'get','action'=>'','legend'=>''));
			$fr->search($this->searchField,@$_GET['cry']);
			$fr->endForm();
		}
	}
}
?>