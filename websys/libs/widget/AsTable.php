<?php
class AsTable
{
	public function startTable($attributes)
	{
		echo "<table ";
		if(isset($attributes))
		{
			if(is_array($attributes))
			{
				echo self::AcAttributes($attributes);
			}
		}
		echo ">".PHP_EOL;
	}

	public function headTable($hdata)
	{
		echo "<thead><tr>";
		if(is_array($hdata))
		{
			self::AcHData($hdata);
		}
		echo "</tr></thead>";
	}
	public function fieldTable($fdata,$attributes=array())
	{
			echo "<tr ";
			foreach($attributes as $key=>$value)
			{
				echo $key."='".$value."' ";
			}
			
			echo ">";
			if(is_array($fdata))
			{
				self::AcFData($fdata);
			}
			echo "</tr>";
	}

	public function endTable()
	{
		echo "</table>";
	}
	function AcAttributes($attributes)
	{
		foreach($attributes as $key => $value)
		{
			echo $key ."=\"$value\" ";
		}
	}
	function AcHData($hdata)
	{
		foreach($hdata as $key => $value)
		{
			echo "<th ";
			if(is_array($value))
			{
				self::AcAttributes($value);
				echo ">$key</th>";
			}else{
				echo ">$value</th>";
			}
		}
	}
	function AcFData($fdata)
	{
			foreach($fdata as $key => $value)
			{
				echo "<td ";
				if(is_array($value))
				{
					self::AcAttributes($value);
					echo ">$key</td>";
				}else{
					echo ">$value</td>";
				}
			}
	}
	
}
?>