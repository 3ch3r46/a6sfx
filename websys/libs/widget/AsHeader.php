<?php
class AsHeader
{
	public $title;
	public $slogan;
	public $url;
	public $htmlOptions=array();
	
	public function __construct($options=array())
	{
		$this->htmlOptions['class'].='page-header';
		foreach($options as $key=>$value)
		{
			$this->{$key}($value);
		}
		$this->run();
	}
	
	public function url($options)
	{
		$this->url=$options;
	}
	
	public function title($options)
	{
		$this->title=$options;
	}
	
	public function useFrame($options)
	{
		if($options==true)
		{
			$this->htmlOptions['class'].=' well';
		}
	}
	
	public function types($options)
	{
		$this->htmlOptions['class'].=' header-'.$options;
	}
	
	public function slogan($options)
	{
		$this->slogan=$options;
	}
	
	public function addClass($options)
	{
		$this->htmlOptions['class'].=' '.$options;
	}
	
	public function run()
	{
		echo "<div ";
		foreach($this->htmlOptions as $key=>$value)
		{
			echo $key."='".$value."' ";
		}
		echo ">".PHP_EOL;
		if(isset($this->title))
		{
			echo "<h1>";
			if(isset($this->url)) echo "<a href='".$this->url."'>";
			echo $this->title;
			if(isset($this->url)) echo "</a>";
			echo "</h1>";
		}
		if(isset($this->slogan))
		{
			echo "<h3><small>";
			echo $this->slogan;
			echo "</small></h3>";
		}
		echo "</div>";
	}
}