<?php
/*
*	aSF	: aSix Form
*	Pembuat	: Moh. Khoirul Anam
*	faedah:
*	fungsi ini digunakan untuk membuat form
*/
class AsActiveForm
{
	private $attributes;
	private $method="post";
	private $fieldset=false;
	private $legend;
	private $count=0;
	public $htmlOptions=array();
	public $ids;
	
	public function __construct($arguments=array())
	{
		foreach($arguments as $key=>$value)
		{
			$this->{$key}($value);
		}
		if(!isset($this->ids)) $this->ids='action';
		//AsHtml::style('form');
		if(!isset($this->htmlOptions['action'])) $this->htmlOptions['action']='';
		if(isset($this->ids)) $this->htmlOptions['action'].='#'.$this->ids;
		echo "<div class='AsForm'";
		if(isset($this->ids)) echo " id='".$this->ids."'";
		echo ">";
		if($fieldset){
			echo "<fieldset>".PHP_EOL;
			echo "<legend>".$this->legend."</legend>".PHP_EOL;
		}elseif(isset($this->legend)){
			echo "<h2>".$this->legend."</h2>".PHP_EOL;
		}
		echo "<form method='$this->method' ";
		foreach($this->htmlOptions as $key=>$value)
		{
			echo $key."='".$value."'";
		}
		echo ">".PHP_EOL;
	}
	
	public function id($value)
	{
		$this->ids=$value;
	}
	
	public function action($options)
	{
		if(is_array($options)){$opti=APPS::createUrl($options);}
		else{$opti=$options;}
		$this->htmlOptions['action']=$opti;
	}
	
	public function type($options)
	{
		$this->htmlOptions['class'].='form-'.$options.' ';
	}
	
	public function method($options)
	{
		$this->method=$options;
	}
	
	public function fieldset($options)
	{
		$this->fieldset=$options;
	}
	
	public function legend($options)
	{
		$this->legend=$options;
	}
	
	public function htmlOptions($options)
	{
		if($options!=null)
		{
			if(is_array($options)){
				foreach($options as $key => $value)
				{
					$this->htmlOptions[$key]=$value." ";
				}
			}
		}
	}
	
	public function end()
	{
		$this->endForm();
	}
	
	private function cAttributes($iattributes)
	{
		foreach($iattributes as $key=>$value)
		{
			echo $key."=\"".$value."\" ";
		}
	}
	
	private function hSpaces($text)
	{
		if(preg_match("/ /", $text))
		{
			return(true);
		}
		return(false);
	}
	function endFieldset()
	{
		if($this->fieldset)
		{
			echo "</fieldset>".PHP_EOL;
			$this->fieldset=false;
		}
	}
	
	public function hiddenField($name,$iattributes)
	{
		echo "<input type='hidden' name='".$name."' ";
		if(isset($iattributes))
		{
			if(is_array($iattributes))
			{
				$this->cAttributes($iattributes);
			}
		}
		echo " />".PHP_EOL;
	}
	
	public function textField($model,$name,$iattributes=null)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label' for='".$name."'>$label</label>".PHP_EOL;
		echo "<div class='controls'><input id='$name' type='text' name='".$model->model."[".$name."]' ";
		echo isset($model->{$name})?" value='".$model->{$name}."' ":'';
		if(isset($htmlOptions))
		{
			foreach($htmlOptions as $key=>$value)
			{
				echo $key."='".$value."' ";
			}
		}
		echo "/>";
		if(isset($hint))
		{
			echo $hint;
		}
		echo "</div>".PHP_EOL;
		echo "</div>".PHP_EOL;
	}
	
	public function passwordField($model,$name,$iattributes=null)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label' for='".$name."'>$label</label>".PHP_EOL;
		echo "<div class='controls'><input id='$name' type='password' name='".$model->model."[".$name."]' ";
		echo isset($model->{$name})?'value="'.$model->{$name}.'"':'';
		if(isset($htmlOptions))
		{
			foreach($htmlOptions as $key=>$value)
			{
				echo $key."='".$value."' ";
			}
		}
		echo "/>";
		if(isset($hint))
		{
			echo $hint;
		}
		echo "</div>".PHP_EOL;
		echo "</div>".PHP_EOL;
	}
	
	public function numberField($model,$name,$iattributes=null)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label' for='".$name."'>$label</label>".PHP_EOL;
		echo "<div class='controls'><input id='$name' type='number' name='".$model->model."[".$name."]' ";
		echo isset($model->{$name})?" value='".$model->{$name}."' ":'';
		if(isset($htmlOptions))
		{
			foreach($htmlOptions as $key=>$value)
			{
				echo $key."='".$value."' ";
			}
		}
		echo "/>";
		if(isset($hint))
		{
			echo $hint;
		}
		echo "</div>".PHP_EOL;
		echo "</div>".PHP_EOL;
	}
	
	public function dateField($model,$name,$iattributes=null)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label' for='".$name."'>$label</label>".PHP_EOL;
		echo "<div class='controls'><input id='$name' type='date' name='".$model->model."[".$name."]' ";
		echo isset($model->{$name})?" value='".$model->{$name}."' ":'';
		if(isset($htmlOptions))
		{
			foreach($htmlOptions as $key=>$value)
			{
				echo $key."='".$value."' ";
			}
		}
		echo "/>";
		if(isset($hint))
		{
			echo $hint;
		}
		echo "</div>".PHP_EOL;
		echo "</div>".PHP_EOL;
	}
	
	public function dateTimeField($model,$name,$iattributes=null)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label' for='".$name."'>$label</label>".PHP_EOL;
		echo "<div class='controls'><input id='$name' type='datetime' name='".$model->model."[".$name."]' ";
		echo isset($model->{$name})?" value='".$model->{$name}."' ":'';
		if(isset($htmlOptions))
		{
			foreach($htmlOptions as $key=>$value)
			{
				echo $key."='".$value."' ";
			}
		}
		echo "/>";
		if(isset($hint))
		{
			echo $hint;
		}
		echo "</div>".PHP_EOL;
		echo "</div>".PHP_EOL;
	}
	
	public function timeField($model,$name,$iattributes=null)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label' for='".$name."'>$label</label>".PHP_EOL;
		echo "<div class='controls'><input id='$name' type='time' name='".$model->model."[".$name."]' ";
		echo isset($model->{$name})?" value='".$model->{$name}."' ":'';
		if(isset($htmlOptions))
		{
			foreach($htmlOptions as $key=>$value)
			{
				echo $key."='".$value."' ";
			}
		}
		echo "/>";
		if(isset($hint))
		{
			echo $hint;
		}
		echo "</div>".PHP_EOL;
		echo "</div>".PHP_EOL;
	}
	
	public function inputField($model,$name,$iattributes=null)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label' for='".$name."'>$label</label>".PHP_EOL;
		echo "<div class='controls'><input id='$name' name='".$model->model."[".$name."]' ";
		echo isset($model->{$name})?" value='".$model->{$name}."' ":'';
		if(isset($htmlOptions))
		{
			foreach($htmlOptions as $key=>$value)
			{
				echo $key."='".$value."' ";
			}
		}
		echo "/>";
		if(isset($hint))
		{
			echo $hint;
		}
		echo "</div>".PHP_EOL;
		echo "</div>".PHP_EOL;
	}
	
	function search($options,$sel=null)
	{
		echo "<table width='100%' class='search table-bordered' cellpadding='0' cellspacing='0' border='0'><tr>".PHP_EOL;
		echo "<td width='60%'><input type='text' class='src' value='".@$_GET['search']."' name='search' placeholder='Search...'></td>";
		echo "<td width='20%'><select name='cry' id='shearch'>";
		if(isset($options))
		{
			if(is_array($options))
			{
				foreach($options as $key=>$value)
				{
					echo "<option ";
					if($sel==$value)
					{
						echo "selected";
					}
						echo " value=\"".$value."\" ";
					echo " >";
					echo $key;
					echo "</option>".PHP_EOL;
				}
			}
		}
		echo "</select></td><td width='20%' class='btn-primary btn-mini'><input type='submit' class='goser' value=''></td>";
		echo "</tr></table>".PHP_EOL;
	}
	
	public function dropDownList($model,$name,$iattributes)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
			if(isset($label))
				echo "<label class='control-label' for='".$name."'>$label</label>";
			echo "<div class='controls'><select id='".$name."' name='".$model->model."[".$name."]' ";
			if(isset($htmlOptions))
			{
				foreach($htmlOptions as $key=>$value)
				{
					echo $key."='".$value."' ";
				}
			}
			echo ">".PHP_EOL;
			if(isset($options))
			{
				if(is_array($options))
				{
					foreach($options as $key=>$value)
					{
						if(is_array($value))
						{
                            echo "<optgroup label='".$key."'>";
							foreach($value as $key1=>$value1)
							{
								echo "<option ";
								if(is_array($value1))
								{
									foreach($value1 as $keq=>$valq)
									{
										echo $keq."='".$valq."' ";
									}
								}
								else
								{
									echo " value=\"".$value1."\" ";
								}
								echo" >";
								echo $key1;
								echo "</option>".PHP_EOL;
							}
							echo "</optgroup>";
						}
						else
						{
							echo "<option ";
							if(is_array($value))
							{
								foreach($value as $keo=>$valo)
								{
									echo $keo."='".$valo."' ";
								}
							}else{
								echo " value=\"".$value."\" ";
							}
							echo " >";
							echo $key;
							echo "</option>".PHP_EOL;
						}
					}
				}
			}
			echo "</select>";
			if(isset($hint))
			{
				echo $hint;
			}
			echo "</div>".PHP_EOL;
		echo "</div>".PHP_EOL;
	}
	
	public function radioInLineOptions($model,$name,$options,$iattributes=null)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label' for='$label'>$label</label>";
		echo "<div class='controls'>".PHP_EOL;
		if(isset($options))
		{
			if(is_array($options))
			{
				foreach($options as $key=>$value)
				{
					echo "<label class='radio inline'><input type='radio' id='radio-".$key."' name='".$model->model."[".$name."]' ";
						if(isset($htmlOptions))
						{
							if(is_array($htmlOptions))
							{
							foreach($htmlOptions as $keo=>$valo)
							{
								echo $keo."='".$valo."' ";
							}
							}
						}
						if(is_array($value))
						{
							foreach($value as $ked=>$vald)
							{
								echo $ked."='".$vald."' ";
								if($value==$model->{$name})
								{
									echo "checked";
								}
							}
						}else{
							echo "value=\"$value\"";
							if($value==$model->{$name})
							{
								echo "checked";
							}
						}
					echo "/>";
					echo "<label class='radios' for='radio-".$key."'>".(is_numeric($key) ? $value : $key)."</label></label>".PHP_EOL;
				}
			}
			else
			{
				trigger_error("you should fill radio argument as array");
				return;
			}
		}
		if(isset($hint))
		{
			echo $hint;
		}
		echo "</div></div>".PHP_EOL;
	}
	public function radioListOptions($model,$name,$options,$iattributes=array())
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label' for='$label'>$label</label>";
			echo "<div class='controls'>".PHP_EOL;
			if(isset($options))
			{
				if(is_array($options))
				{
					foreach($options as $key=>$value)
					{
						echo "<label class='radio'><input type='radio' id='radio-".$key."'  name='".$model->model."[".$name."]' ";
						if(isset($htmlOptions))
						{
							if(is_array($htmlOptions))
							{
								foreach($htmlOptions as $keo=>$valo)
								{
									echo $keo."='".$valo."' ";
								}
							}
						}
						if(is_array($value))
						{
							foreach($value as $ked=>$vald)
							{
								echo $ked."='".$vald."' ";
								if($vald==$model->{$name})
								{
									echo "checked";
								}
							}
						}else{
							echo "value=\"$value\"";
							if($value==$model->{$name})
							{
								echo "checked";
							}
						}
						echo "/>";
						echo "<label for='radio-".$key."'>".(is_numeric($key) ? $value : $key)."</label></label>".PHP_EOL;
					}
				}
				else
				{
				trigger_error("you should fill radio argument as array");
						return;
				}
			}
			if(isset($hint))
				{
				echo $hint;
			}
			echo "</div></div>".PHP_EOL;
	}
			
	public function checkBoxInLineOptions($model,$name,$options,$iattributes=null)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label'>$label</label>";
		echo "<div class='controls'>".PHP_EOL;
		if(isset($options))
		{
			if(is_array($options))
			{
				foreach($options as $key=>$value)
				{
					echo "<label class='checkbox inline'>";
					echo "<input type='checkbox' id='check-".$key."' name='".$model->model."[".$name."]' ";
					if(isset($htmlOptions))
					{
						if(is_array($htmlOptions))
						{
							foreach($htmlOptions as $keo=>$valo)
							{
								echo $keo."='".$valo."' ";
							}
						}
					}
					if(is_array($value))
					{
							foreach($value as $ked=>$vald)
							{
								echo $ked."='".$vald."' ";
							}
					}else{
						echo "value='$value'";
					}
					echo"/>".PHP_EOL;
					echo "<label for='check-".$key."'>".(is_numeric($key) ? value : $key)."</label>".PHP_EOL;
					echo "</label>".PHP_EOL;
				}
			}
			else
			{
				trigger_error("you should fill checkbox argument as array");
				return;
			}
		}
		echo "</div></div>".PHP_EOL;
	}
	
	public function checkBoxListOptions($model,$name,$options,$iattributes=null)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label'>$label</label>";
		echo "<div class='controls'>".PHP_EOL;
		if(isset($options))
		{
			if(is_array($options))
			{
				foreach($options as $key=>$value)
				{
					echo "<label class='checkbox'>";
					echo "<input type='checkbox' id='check-".$key."' name='".$model->model."[".$name."]' ";
					if(isset($htmlOptions))
					{
						if(is_array($htmlOptions))
						{
							foreach($htmlOptions as $keo=>$valo)
							{
								echo $keo."='".$valo."' ";
							}
						}
					}
					if(is_array($value))
					{
							foreach($value as $ked=>$vald)
							{
								echo $ked."='".$vald."' ";
							}
					}else{
						echo "value='$value'";
					}
					echo"/>".PHP_EOL;
					echo "<label for='check-".$key."'>".(is_numeric($key) ? value : $key)."</label>".PHP_EOL;
					echo "</label>".PHP_EOL;
				}
			}else{
				trigger_error("you should fill checkbox argument as array");
				return;
			}
		}
		echo "</div></div>".PHP_EOL;
	}
	
	public function textArea($model,$name,$iattributes=null)
	{
		extract($iattributes);
		echo "<div class='control-group'>".PHP_EOL;
		if(isset($label))
			echo "<label class='control-label' for='".$name."'>$label</label>".PHP_EOL;
		echo "<div class='controls'><textarea id='".$name."' name='".$model->model."[".$name."]' ";
		if(isset($htmlOptions))
		{
			if(is_array($htmlOptions))
			{
				foreach($htmlOptions as $kes=>$vals)
				{
					echo $kes."='".$vals."' ";
				}
			}
		}
		echo ">";
		echo isset($model->{$name})?$model->{$name}:'';
		echo "</textarea></div>".PHP_EOL;
		echo "</div>".PHP_EOL;
		$this->count++;
	}
	function button($label='button',$iattributes=null,$tipe='submit')
	{
		echo "<button class='button' type='$tipe' ";
		if(isset($iattributes))
		{
			if(is_array($iattributes))
			{
				$this->cAttributes($iattributes);
			}
		}
		echo "> $label </button>".PHP_EOL;
	}
    public function file($label,$id,$iattributes)
	{
		echo "<div class='control-group'>".PHP_EOL;
                echo "<script>function getFile".$id."(){ document.getElementById('file-".$id."').click(); }</script>";
		if(isset($label))
		echo "<label class='control-label' for='file-".$id."'>$label</label>".PHP_EOL;
		echo "<button type='button' onclick='getFile".$id."()' class='btn btn-inverse btn-small'>Select Your ".$label." To Upload</button>";
		echo "<input id='file-".$id."' type='file' ";
		if(isset($iattributes))
		{
			if(is_array($iattributes))
			{
				echo $this->cAttributes($iattributes);
			}
		}
		echo " style='visibility:hidden;width:0px;'/>";                
		echo "</div>".PHP_EOL;
		$this->count++;
	}
	function endForm()
	{
		echo "</form>".PHP_EOL;
                if($this->fieldset)
		{
			echo "</fieldset>".PHP_EOL;
		}
		echo "</div>".PHP_EOL;
	}
        
        function label($isi=null){
            echo "<label>$isi</label>";
        }
}
?>