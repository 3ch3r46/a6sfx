<?php
class AsMenu
{
	public $type;
	public $stacked;
	public $items=array();
	public $htmlOptions;
	public $useFrame=false;
	
	public function __construct($options)
	{
		$this->htmlOptions['class']="nav ";
		foreach($options as $key=>$value)
		{
			$this->{$key}($value);
		}
		$this->run();
	}
	
	public function useFrame($options)
	{
		$this->useFrame=$options;
	}
	
	public function stacked($options)
	{
		if($options==true)
		{
			$this->htmlOptions['class'].=" nav-stacked";
		}
	}
	
	public function type($options)
	{
		$this->htmlOptions['class'].=' nav-'.$options;
	}
	
	public function addClass($options)
	{
		$this->htmlOptions['class'].=' '.$options;
	}
	
	public function items($item)
	{
		foreach($item as $value)
		{
			if(isset($value['visible']))
			{
				if($value['visible']==true){
				if(is_array($value))
				{
					if(isset($value['type'])) $class.=" nav-".$value['type'];
					if(isset($value['class'])) $class.=" ".$value['class'];
					$menu="   <li";
					if(isset($class)) $menu.=" class='".$class."'";
					$menu.=">".PHP_EOL;
					if(isset($value['url'])){
						if(is_array($value['url'])){
							$menu.="      <a href='".APPS::createUrl($value['url'])."'>";
						}else{
							$menu.="      <a href='".$value['url']."'>";
						}
					}
					if(isset($value['icon'])) $menu.="<i class='icon-".$value['icon']."'></i> ";
					if(isset($value['label'])) $menu.=$value['label'];
					if(isset($value['items'])) $menu.="<span class='caret'></span>";
					if(isset($value['url'])) $menu.="</a>".PHP_EOL;
					$menu.="</li>";
				}elseif($itemns=="---")
				{
					$menu.="   <li class='divider'></li>".PHP_EOL;
				}
				$this->items[]=$menu;
				}
			$menu.="   </li>".PHP_EOL;
			}else{
				if(is_array($value))
				{
					if(isset($value['type'])) $class.=" nav-".$value['type'];
					if(isset($value['class'])) $class.=" ".$value['class'];
					$menu="   <li";
					if(isset($class)) $menu.=" class='".$class."'";
					$menu.=">".PHP_EOL;
					if(isset($value['url'])){
						if(is_array($value['url'])){
							$menu.="      <a href='".APPS::createUrl($value['url'])."'>";
						}else{
							$menu.="      <a href='".$value['url']."'>";
						}
					}
					if(isset($value['icon'])) $menu.="<i class='icon-".$value['icon']."'></i> ";
					if(isset($value['label'])) $menu.=$value['label'];
					if(isset($value['items'])) $menu.="<span class='caret'></span>";
					if(isset($value['url'])) $menu.="</a>".PHP_EOL;
					$menu.="</li>";
				}elseif($itemns=="---")
				{
					$menu.="   <li class='divider'></li>".PHP_EOL;
				}
				$this->items[]=$menu;
			}
		}
	}
	
	public function run()
	{
		if($this->useFrame==true) echo "<div class='well'>";
		echo "<ul ";
		foreach($this->htmlOptions as $key=>$value)
		{
			echo $key."='".$value."' ";
		}
		echo ">".PHP_EOL;
		
		foreach($this->items as $item)
		{
			echo $item;
		}
		echo "</ul>".PHP_EOL;
		if($this->useFrame==true) echo "</div>";
	}
}