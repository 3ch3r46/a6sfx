<?php
class AsNavbar
{
	public $type;
	public $brand;
	public $brandUrl;
	public $brandOptions;
	public $useFixed;
	public $fixed=" navbar-fixed-top";
	public $collapse=false;
	public $items=array();
	public $htmlOptions;
	
	public function __construct($options)
	{
		$this->htmlOptions['class']="navbar";
		foreach($options as $key=>$value)
		{
			$this->{$key}($value);
		}
		$this->htmlOptions['class'].=$this->fixed;
		$this->run();
	}
	
	public function type($options)
	{
		$this->htmlOptions['class'].=' navbar-'.$options;
	}
	public function brand($options)
	{
		$this->brand=$options;
	}
	public function brandUrl($options)
	{
		$this->brandUrl=$options;
	}
	public function brandOptions($options)
	{
		$this->brandOptions=$options;
	}
	public function fixed($options)
	{
		$this->useFixed=true;
		$this->fixed=" navbar-fixed-".$options;
	}
	public function collapse($bol)
	{
		$this->collapse=$bol;
	}
	
	public function items($item)
	{
		foreach($item as $value)
		{
			if(isset($value['visible']))
			{
				if($value['visible']==true){
				$menu="   <li";
				if(isset($value['items'])) $menu.=" class='dropdown'";
				$menu.=">".PHP_EOL;
				if(is_array($value))
				{
					if(isset($value['url'])){
						$menu.="      <a ".(isset($value['items'])?"class='dropdown-toggle' data-toggle='dropdown'":'')." href='";
						if(is_array($value['url'])) $menu.=APPS::createUrl($value['url']);
						else $menu.=$value['url'];
						if(isset($value['htmlOptions']))
						{
							foreach($value['htmlOptions'] as $key=>$values)
							{
								$menu.=" ".$key."='".$values."'";
							}
						}
						$menu.="'>";
					}
					if(isset($value['icon'])) $menu.="<i class='icon-".$value['icon']."'></i> ";
					if(isset($value['label'])) $menu.=$value['label'];
					if(isset($value['badge'])) $menu.="<span class='badge pull-right'>".$value['badge']."</span>";
					if(isset($value['items'])) $menu.="<span class='caret'></span>";
					if(isset($value['url'])) $menu.="</a>".PHP_EOL;
					if(isset($value['items'])){
						$menu.="      <ul class='dropdown-menu'>".PHP_EOL;
						if(is_array($value['items']))
						{
							foreach($value['items'] as $itemns)
							{
								if(is_array($itemns))
								{
									if(isset($itemns['visible']))
									{
										if($items['visible']==true)
										{
											$menu.="\t\t<li";
											if(isset($itemns['type'])) $menu.=" class='nav-".$itemns['type']."'";
											$menu.=">".PHP_EOL;
											if(isset($itemns['url'])){
												$menu.="\t\t\t<a tabindex='-1' href='";
												if(is_array($itemns['url'])) $menu.=APPS::createUrl($itemns['url']);
												else $menu.=$itemns['url'];
												if(isset($itemns['htmlOptions']))
												{
													foreach($itemns['htmlOptions'] as $key=>$values)
													{
														$menu.=" ".$key."='".$values."'";
													}
												}
												$menu.="'>";
											}
											
											if(isset($itemns['icon'])) $menu.="<i class='icon-".$itemns['icon']."'></i> ";
											if(isset($itemns['label'])) $menu.=$itemns['label'];
											if(isset($itemns['badge'])) $menu.="<span class='badge pull-right'>".$itemns['badge']."</span>";
											if(isset($itemns['url'])) $menu.="</a>".PHP_EOL;
											$menu.="         </li>".PHP_EOL;
										}
									}else{
										$menu.="         <li";
										if(isset($itemns['type'])) $menu.=" class='nav-".$itemns['type']."'";
										$menu.=">".PHP_EOL;
										if(isset($itemns['url'])){
												$menu.="\t\t\t<a tabindex='-1' href='";
												if(is_array($itemns['url'])) $menu.=APPS::createUrl($itemns['url']);
												else $menu.=$itemns['url'];
												if(isset($itemns['htmlOptions']))
												{
													foreach($itemns['htmlOptions'] as $key=>$values)
													{
														$menu.=" ".$key."='".$values."'";
													}
												}
												$menu.="'>";
											}
										if(isset($itemns['icon'])) $menu.="<i class='icon-".$itemns['icon']."'></i> ";
										if(isset($itemns['label'])) $menu.=$itemns['label'];
										if(isset($itemns['badge'])) $menu.="<span class='badge pull-right'>".$itemns['badge']."</span>";
										if(isset($itemns['url'])) $menu.="</a>".PHP_EOL;
										$menu.="         </li>".PHP_EOL;
									}
								}elseif($itemns=="---")
								{
									$menu.="         <li class='divider'></li>".PHP_EOL;
								}
							}
						}
						$menu.="      </ul>".PHP_EOL;
					}
				}
				$this->items[]=$menu;
			}
			$menu.="   </li>".PHP_EOL;
			}else{
				$menu="   <li";
				if(isset($value['items'])) $menu.=" class='dropdown'";
				$menu.=">".PHP_EOL;
				if(is_array($value))
				{
					if(isset($value['url'])){
						$menu.="      <a ".(isset($value['items'])?"class='dropdown-toggle' data-toggle='dropdown'":'')." href='";
						if(is_array($value['url'])) $menu.=APPS::createUrl($value['url']);
						else $menu.=$value['url'];
						if(isset($value['htmlOptions']))
						{
							foreach($value['htmlOptions'] as $key=>$values)
							{
								$menu.=" ".$key."='".$values."'";
							}
						}
						$menu.="'>";
					}
					if(isset($value['icon'])) $menu.="<i class='icon-".$value['icon']."'></i> ";
					if(isset($value['label'])) $menu.=$value['label'];
					if(isset($value['badge'])) $menu.="<span class='badge pull-right'>".$value['badge']."</span>";
					if(isset($value['items'])) $menu.="<span class='caret'></span>";
					if(isset($value['url'])) $menu.="</a>".PHP_EOL;
					if(isset($value['items'])){
						$menu.="      <ul class='dropdown-menu'>".PHP_EOL;
						if(is_array($value['items']))
						{
							foreach($value['items'] as $itemns)
							{
								if(is_array($itemns))
								{
								if(isset($itemns['visible']))
									{
										if($items['visible']==true)
										{
											$menu.="         <li";
											if(isset($itemns['type'])) $menu.=" class='nav-".$itemns['type']."'";
											$menu.=">".PHP_EOL;
											if(isset($itemns['url'])){
												$menu.="\t\t\t<a tabindex='-1' href='";
												if(is_array($itemns['url'])) $menu.=APPS::createUrl($itemns['url']);
												else $menu.=$itemns['url'];
												if(isset($itemns['htmlOptions']))
												{
													foreach($itemns['htmlOptions'] as $key=>$values)
													{
														$menu.=" ".$key."='".$values."'";
													}
												}
												$menu.="'>";
											}
											if(isset($itemns['icon'])) $menu.="<i class='icon-".$itemns['icon']."'></i> ";
											if(isset($itemns['label'])) $menu.=$itemns['label'];
											if(isset($itemns['badge'])) $menu.="<span class='badge pull-right'>".$itemns['badge']."</span>";
											if(isset($itemns['url'])) $menu.="</a>".PHP_EOL;
											$menu.="         </li>".PHP_EOL;
										}
									}else{
										$menu.="         <li";
										if(isset($itemns['type'])) $menu.=" class='nav-".$itemns['type']."'";
										if(isset($itemns['htmlOptions']))
										$menu.=">".PHP_EOL;
										if(isset($itemns['url'])){
												$menu.="\t\t\t<a tabindex='-1' href='";
												if(is_array($itemns['url'])) $menu.=APPS::createUrl($itemns['url']);
												else $menu.=$itemns['url'];
												{
													foreach($itemns['htmlOptions'] as $key=>$values)
													{
														$menu.=" ".$key."='".$values."'";
													}
												}
												$menu.="'>";
											}
										if(isset($itemns['icon'])) $menu.="<i class='icon-".$itemns['icon']."'></i> ";
										if(isset($itemns['label'])) $menu.=$itemns['label'];
										if(isset($itemns['badge'])) $menu.="<span class='badge pull-right'>".$itemns['badge']."</span>";
										if(isset($itemns['url'])) $menu.="</a>".PHP_EOL;
										$menu.="         </li>".PHP_EOL;
									}
								}elseif($itemns=="---")
								{
									$menu.="         <li class='divider'></li>".PHP_EOL;
								}
							}
						}
						$menu.="      </ul>".PHP_EOL;
					}
				}elseif($value=="---")
				{
					$menu.="     <li class='divider-vertical'></li>".PHP_EOL;
				}
				$menu.="   </li>".PHP_EOL;
				$this->items[]=$menu;
			}
		}
	}
	
	public function run()
	{
		echo "<div ";
		foreach($this->htmlOptions as $key=>$value)
		{
			echo $key."='".$value."' ";
		}
		echo ">
	<div class='navbar-inner'>
		<div class='container'>".PHP_EOL;
		$collapse=null;
		if($this->collapse==true)
		{
			$colsp=rand(000,999);
			echo "<a class='btn btn-navbar' data-toggle='collapse' data-target='#navbar_collapse_show_$colsp'>
			<span class='icon-bar'></span>
			<span class='icon-bar'></span>
			<span class='icon-bar'></span>
			</a>".PHP_EOL;
			$collapse="id='navbar_collapse_show_$colsp'";
			APPS::loadJScript('collapse.js');
		}
		
		if(isset($this->brand)) echo "<a href='".($this->brandUrl!=null?$this->brandUrl:"#")."' class='brand'>".$this->brand."</a>";
		
		echo "<div class='nav-collapse' $collapse>".PHP_EOL;
		echo "<ul class='nav'>".PHP_EOL;
		foreach($this->items as $item)
		{
			echo $item;
		}
		echo "</ul>".PHP_EOL;
		echo "</div>".PHP_EOL;
		
		echo "</div></div></div>".PHP_EOL;
		APPS::loadJScript('dropdown.js');
	}
}