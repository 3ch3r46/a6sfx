<?php
class AsView {

    function __construct() {
        //parent::__construct();
    }
    
    public function startView($judul='Judul View',$sts=true)
    {
        if($sts==false){
            echo "<div class='detail'><h1>$judul</h1>".PHP_EOL;
            $this->filset=false;
        }else{
            echo "<fieldset><legend>$judul</legend>".PHP_EOL;
            $this->filset=true;
        }
    }
    public function isi($label,$isi)
    {
        echo "<div class='isi'><label class='label'>$label : </label>
            <div class='isian'>$isi</div></div>".PHP_EOL;
    }
    public function endView()
    {
        if($this->filset==false)
        {
            echo "</div>".PHP_EOL;
        }else{
            echo "</fieldset>".PHP_EOL;
        }
    }
}