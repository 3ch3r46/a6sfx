<?php
class AsButton
{
	public $_type;
	public $_class='btn ';
	public $_url;
	public $_useUrl;
	public $_label='Action';
	public $_size;
	public $_action='button';
	public $_icon='';
	public $_htmlOptions;
	
	public function __construct($options=array())
	{
		if($options!=null)
		{
			foreach($options as $key=>$value)
			{
				$this->{$key}($value);
			}
		}
		$this->bindButton();
	}
	
	public function action($options)
	{
		$this->_action=$options;
	}
	
	public function btnClass($options)
	{
		$this->_class.=$options;
	}
	
	public function url($options)
	{
		$this->_useUrl=true;
		if(is_array($options))
			$this->_url=APPS::createUrl($options);
		else
			$this->_url=$options;
	}
	
	public function label($options)
	{
		$this->_label=$options;
	}
	
	public function icon($options)
	{
		$this->_icon="<i class='icon-".$options."'></i> ";
	}
	
	public function size($options)
	{
		$this->_size=' btn-'.$options;
	}
	
	public function type($options)
	{
		$this->_type=' btn-'.$options;
	}
	
	public function htmlOptions($options)
	{
		$this->_htmlOptions='';
		foreach($options as $key=>$value)
		{
			$this->_htmlOptions.=$key."='".$value."' ";
		}
	}
	
	public function bindButton()
	{
		$btn='';
		if($this->_useUrl==true)
		{
			$btn.="<a href='".$this->_url."' class='".$this->_class.$this->_size.$this->_type."' ".$this->_htmlOptions.">";
			$btn.=$this->_icon.$this->_label;
			$btn.="</a>";
		}else $btn.="<button type='".$this->_action."' class='".$this->_class.$this->_size.$this->_type."' ".$this->_htmlOptions.">".$this->_icon.$this->_label."</button>";

		echo $btn;
	}
}