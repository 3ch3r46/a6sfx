<?php

class AsBreadcrumbs {

	public $breadcrumbs;
	private $pointer = '/';
	private $url;
	private $parts;

	function __construct() {
		$this->setParts();
		$this->setUrl();
		$this->breadcrumbs = "<a href='".ROOT."' class='btn-link' >Home</a> ";
		$this->crumbs();
		echo "<div class='inner'><div class='breadcrumb'>";
		echo $this->breadcrumbs;
		echo "
	</div>
</div>";
	}

	public function setUrl()
	{
		$url = rtrim(ROOT,"/");
		$this->url = $url;
	}

	public function setPointer($pointer)
	{
		$this->pointer=$pointer;
	}

	public function setParts()
	{
		$parts = explode('/',$_GET['url']);
		$this->parts=$parts;
	}

	public function crumbs()
	{
		$ad=count($this->parts);
		$ads=0;
		foreach($this->parts as $parts)
		{
			$ads++;
			$parts = ucfirst($parts);
			if($ads==$ad)
			{
				$this->breadcrumbs .= "<span class='divider'>".$this->pointer."</span> <a class='active'>".$parts."</a> ";
			}else{
				$this->url .="/".$parts;
				$this->breadcrumbs .= "<span class='divider'>".$this->pointer."</span> <a href='".$this->url."' class='btn-link'>".$parts."</a> ";
			}
		}
	}
}