<?php
class Menu {

    function __construct($adata) {
        echo "<ul>".PHP_EOL;
        if(is_array($adata))
        {
            $this->setMenu($adata);
        }
        echo "</ul>".PHP_EOL;
    }

    public function setMenu($adata)
    {
        if(is_array($adata))
        {
            foreach($adata as $key=>$value)
            {
                if(is_array($value))
                {
                    echo "<li><a href='#'>$key</a><ul><div class='spar'></div>".PHP_EOL;
                    foreach($value as $key1=>$value1)
                    {
                        if(is_array($value1)){
                            echo "<li><a href='#'>$key1</a><ul><div class='spad'></div>".PHP_EOL;
                            foreach($value1 as $key2=>$value2)
                            {
                                echo "<li><a href='".URL."$value2'>$key2</a></li>".PHP_EOL;
                            }
                            echo "</ul></li>".PHP_EOL;
                        }else{
                            echo "<li><a href='".URL."$value1'>$key1</a></li>".PHP_EOL;                            
                        }

                    }
                    echo "</ul></li>".PHP_EOL;
                }
                else{
                echo "<li><a href='".URL."$value'>$key</a></li>".PHP_EOL;
                }
            }
        }
    }
}
?>
