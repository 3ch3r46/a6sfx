<?php
class AsHeroUnit
{
	public $_heading;
	public $_headingOptions=array();
	public $htmlOptions=array();
	
	public function __construct($options=null)
	{
		$this->htmlOptions['class']='hero-unit';
		if(isset($options))
		{
			foreach($options as $key=>$val)
			{
				$this->{$key}($val);
			}
		}
		echo "<div ";
		foreach($this->htmlOptions as $key=>$value)
		{
			echo $key."='".$value."' ";
		}
		echo ">";
		if(isset($this->_heading))
		{
			echo "<h1";
			if(isset($this->_headingOptions))
			{
				foreach($this->_headingOptions as $key=>$value)
				{
					echo " ".$key."='".$value."'";
				}
			}
			echo ">";
			echo $this->_heading;
			echo "</h1>";
		}
	}
	
	public function heading($options)
	{
		$this->_heading=$options;
	}
	public function headingOptions($options)
	{
		$this->_headingOptions=$options;
	}
	
	public function end()
	{
		echo "</div>";
	}
}