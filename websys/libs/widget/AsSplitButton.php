<?php
class AsSplitButton
{
	public $_type;
	public $_class='btn ';
	public $_url;
	public $_useUrl=false;
	public $_label='Action';
	public $_size;
	public $_action='button';
	public $_icon='';
	public $_htmlOptions;
	public $_items=array();
	public $_itemsData=array();
	
	public function __construct($options=array())
	{
		$this->_htmlOptions['class']='btn';
		if($options!=null)
		{
			foreach($options as $key=>$value)
			{
				$this->{$key}($value);
			}
		}
		$this->bindButton();
	}
	
	public function action($options)
	{
		$this->_action=$options;
	}
	
	public function btnClass($options)
	{
		$this->_htmlOptions['class'].=' '.$options;
	}
	
	public function url($options)
	{
		$this->_useUrl=true;
		$this->_htmlOptions['href']=$options;
	}
	
	public function label($options)
	{
		$this->_label=$options;
	}
	
	public function icon($options)
	{
		$this->_icon="<i class='icon-".$options."'></i> ";
	}
	
	public function size($options)
	{
		$this->_htmlOptions['class'].=' btn-'.$options;
	}
	
	public function type($options)
	{
		$this->_htmlOptions['class'].=' btn-'.$options;
		$this->_type=' btn-'.$options;
	}
	
	public function htmlOptions($options)
	{
		$this->_htmlOptions=$options;
	}
	
	public function items($options)
	{
		$this->_items=$options;
		$this->bindItems();
	}
	
	public function bindItems()
	{
		foreach($this->_items as $value){
			$menu='';
			if(is_array($value))
			{
				if(isset($value['visible']))
				{
					if($value['visible']==true)
					{
						$menu.="<li";
						if(isset($value['type'])) $menu.=" class='nav-".$value['type']."'";
						$menu.=">".PHP_EOL;
						if(isset($value['url'])) $menu.="<a tabindex='-1' href='".$value['url']."'>";
						if(isset($value['icon'])) $menu.="<i class='icon-".$value['icon']."'></i> ";
						if(isset($value['label'])) $menu.=$value['label'];
						if(isset($value['badge'])) $menu.="<span class='badge pull-right'>".$value['badge']."</span>";
						if(isset($value['url'])) $menu.="</a>".PHP_EOL;
						$menu.="</li>".PHP_EOL;
					}
				}else{
					$menu.="<li";
					if(isset($value['type'])) $menu.=" class='nav-".$value['type']."'";
					$menu.=">".PHP_EOL;
					if(isset($value['url'])) $menu.="<a tabindex='-1' href='".$value['url']."'>";
					if(isset($value['icon'])) $menu.="<i class='icon-".$value['icon']."'></i> ";
					if(isset($value['label'])) $menu.=$value['label'];
					if(isset($value['badge'])) $menu.="<span class='badge pull-right'>".$value['badge']."</span>";
					if(isset($value['url'])) $menu.="</a>".PHP_EOL;
					$menu.="</li>".PHP_EOL;
				}
			}elseif($value=="---")
			{
				$menu.="<li class='divider'></li>".PHP_EOL;
			}
			$this->_itemsData[]=$menu;
		}
	}
	
	public function bindButton()
	{
		$btn='<!-- split button -->';
		$btn.="<div class='btn-group'>".PHP_EOL;
		if($this->_useUrl==true)
		{
			$btn.="<a ";
			foreach($this->_htmlOptions as $key=>$value)
			{
				$btn.=$key."='".$value."' ";
			}
			$btn.=">";
			$btn.=$this->_icon.$this->_label;
			$btn.="</a>";
		}else{
			$btn.="<button type='".$this->_action."' ";
			foreach($this->_htmlOptions as $key=>$value)
			{
				$btn.=$key."='".$value."' ";
			}
			$btn.=">".$this->_icon.$this->_label."</button>";
		}
		$btn.="<button type='button' class='btn ".$this->_type." dropdown-toggle' data-toggle='dropdown'>";
		$btn.="<span class='caret'></span>";
		$btn.="</button>";
		$btn.="<ul class='dropdown-menu'>";
		foreach($this->_itemsData as $item)
		{
			$btn.=$item;
		}
		$btn.="</ul>";
		$btn.="</div>";
	
		echo $btn;
	}
	
}
?>