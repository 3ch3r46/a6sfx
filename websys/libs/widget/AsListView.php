<?php
class AsListView
{
	public function __construct($model,$view)
	{
		$this->model=$model;
		$this->view=$view;
		$this->showView();
		$this->navg();
	}
	
	public function showView()
	{
		AsHtml::style('listView');
		foreach($this->model->readData() as $a)
		{
		extract($a);
		require $_SERVER['DOCUMENT_ROOT'].ROOT.'websys/views/'.strtolower($this->model->model).'/'.$this->view.'.php';
		}
	}
	
	public function navg()
	{
		echo $this->model->navigation(array('url'=>$this->model->model.'/index'));
	}
}