<?php
class AsDetailView
{
	public $dataProvider;
	public $htmlOptions=array();
	public $items=array();
	public $model;
	public $fieldHeading;
	
	public function __construct($model,$options=array())
	{
		$this->model=$model;
		$this->htmlOptions['class']='detail-view table';
		
		foreach($options as $key=>$value)
		{
			$this->{$key}($value);
		}
		$this->run();
	}
	
	public function types($options)
	{
		$opt=explode(' ',$options);
		foreach($opt as $value)
		{
			$this->htmlOptions['class'].=" table-".$value;
		}
	}
	
	public function items($options)
	{
		$this->items=$options;
	}
	
	public function heading($options)
	{
		$this->fieldHeading=$options;
	}
	
	public function type($options)
	{
		$opt=explode(' ',$options);
		foreach($opt as $value)
		{
			$this->htmlOptions['class'].=' table-'.$value;
		}
	}
	
	public function run()
	{
		$model=$this->model;
		echo "<h1>".$this->model->model." #".$model->{$this->fieldHeading}."</h1>".PHP_EOL;
		echo "<table ";
		foreach($this->htmlOptions as $key=>$value)
		{
			echo $key."='".$value."' ";
		}
		echo ">".PHP_EOL;
		echo "<tbody>".PHP_EOL;
		
		foreach($this->items as $value)
		{
			echo "<tr>";
			echo "<th>".$value['label']."</th>";
			echo "<td>".$model->{$value['name']}."</td>";
			echo "</tr>";
		}
		
		echo "</tbody>".PHP_EOL;
		echo "</table>";
	}
}