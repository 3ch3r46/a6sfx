<?php
class AsUploadFile
{
	private static $nameFile;
	private static $modelName;
	
	/**
	 * Get Instance data file
	 * @param Model $model
	 * @param string $name
	 */
	public static function getInstance($model,$name)
	{
		self::$modelName=$model->model;
		self::$nameFile=$name;
	}
	
	/**
	 * GetName from file
	 * @return string $name and filename is original name from file
	 */
	public function getName()
	{
		$name=$_FILES[self::$modelName]['name'][self::$nameFile];
		return $name;
	}
	
	/**
	 * getTempName from file
	 * @return string $tmp_name and tmp_name is a temporary file
	 */
	public function getTempName()
	{
		$tmp_name=$_FILES[self::$modelName]['tmp_name'][self::$nameFile];
		return $tmp_name;
	}
	
	/**
	 * getType from file
	 * @return string $type a type file
	 */
	public function getType()
	{
		$type=$_FILES[self::$modelName]['type'][self::$nameFile];
		return $type;
	}
	
	/**
	 * getSize file
	 * @return integer $size
	 */
	public function getSize()
	{
		$size=$_FILES[self::$modelName]['size'][self::$nameFile];
		return $size;
	}
	
	/**
	 * getExtension file
	 * @return string $extention is a extension File
	 * @example .jpg .png .docx .jpeg .gif etc
	 */
	public function getExtension()
	{
		$pos=strrpos($this->getName(), '.');
		$extention=substr($this->getName(), $pos,$pos+10);
		return $extention;
	}
	
	/**
	 * Save the uploaded file
	 * @param string $file is a directory file and filename
	 * @param boolean $deleteTempFile
	 * @example KhAS::app()->webAppDir.'images/'.$model->file
	 */
	public function saveAs($file,$deleteTempFile=true)
	{
		if($deleteTempFile){
			return move_uploaded_file($this->getTempName(), $file);
		}elseif(is_uploaded_file($this->getTempName()))
		{
			return copy($this->getTempName(),$file);
		}else{
			return false;
		}
	}
}