<?php
class AsHtml {
    public static function style($style)
    {
        echo "<link href='".ROOT."public/css/".$style.".css' rel='stylesheet' />".PHP_EOL;
    }
    public static function favicon($img)
    {
        echo "<link href='".ROOT."public/images/".$img."' rel='shortcut icon' />".PHP_EOL;
    }
    public static function jsLoad($js)
    {
        echo "<script src=\"".ROOT."public/js/".$js.".js\" type=\"text/javascript\"></script>".PHP_EOL;
    }
    public static function script($isi)
    {
        echo "<script type='text/javascript'>".PHP_EOL;
        if(isset($isi))
        {
            if(is_array($isi))
            {
                foreach($isi as $key=>$value)
                {
                    echo $value.PHP_EOL;
                }
            }
           	else
           	{
           		echo $isi.PHP_EOL;
           	}
        }
        echo "</script>".PHP_EOL;
    }
    public static function meta($name,$content)
    {
        $meta = "<meta name='$name' content='";
        if(is_array($content))
        {
            $end=end(array_keys($content));
            foreach($content as $key=>$value)
            {
                if($key==$end)
                {
                    $meta .='$value';
                }else{
                    $meta .='$value,';
                }
            }
        }else{
            $meta .=$content;
        }
        $meta .="' />";
        echo $meta;
    }
    public static function title($title)
    {
        echo "<title>$title - ".WebName."</title>".PHP_EOL;
    }
    public static function link($link,$values,$tipe='text',$attribute=null)
    {
        if($tipe=='button')
        {
            $value = "<button type='button'>$values</button>";
        }else{
            $value = $values;
        }
        $as="<a href=\"$link\" ";
        if(isset($attribute))
        {
            foreach($attribute as $key=>$valued)
            {
                $as.="$key='$valued'";
            }
        }
        $as.=">";
        $as.=$value;
        $as.="</a>".PHP_EOL;
        return $as;
    }
    public static function div($isi,$attribute)
    {
        $div = "<div ";
        if(isset($attribute))
        {
            if(is_array($attribute))
            {
                foreach($attribute as $key=>$value)
                {
                    $div .="$key='$value' ";
                }
            }
        }
        $div .=">$isi</div>".PHP_EOL;
        echo $div;
    }
    public static function startDiv($attribute)
    {
        $div = "<div ";
        if(isset($attribute))
        {
            if(is_array($attribute))
            {
                foreach($attribute as $key=>$value)
                {
                    $div .="$key='$value' ";
                }
            }
        }
        $div .=">".PHP_EOL;
        echo $div;
    }
    public static function endDiv()
    {
        echo "</div>".PHP_EOL;
    }
    public static function img($isi,$attribute=null)
    {
        $img = "<img src='".ROOT."public/images/".$isi."' ";
        if(isset($attribute))
        {
            if(is_array($attribute))
            {
                foreach($attribute as $key=>$value)
                {
                    $img .="$key='$value' ";
                }
            }
        }
        $img .="/>".PHP_EOL;
        return $img;
    }
    public static function h($tipe,$values,$attribute=null)
    {
        $h = "<h$tipe ";
        if(isset($attribute))
        {
            if(is_array($attribute))
            {
                foreach($attribute as $key=>$value)
                {
                    $h .="$key='$value' ";
                }
            }
        }
        $h .=">$values</h$tipe>".PHP_EOL;
        echo $h;
    }
    public static function br()
    {
        return "<br/>".PHP_EOL;
    }
    public static function center($value)
    {
        echo "<center>$value</center>".PHP_EOL;
    }
    public static function html()
    {
        echo "<html>".PHP_EOL;
    }
    public static function ehtml()
    {
        echo "</html>".PHP_EOL;
    }
    public static function head()
    {
        echo "<head>".PHP_EOL;
    }
    public static function ehead()
    {
        echo "</head>".PHP_EOL;
    }
    public static function body($attrib=null)
    {
        $b ="<body ";
        if(is_array($attrib))
        {
            foreach($attrib as $key=>$value)
            {
                $b .="$key='$value'";
            }
        }
        $b .=">".PHP_EOL;
        echo $b;
    }
    public static function ebody()
    {
        echo "</body>".PHP_EOL;
    }
    public static function button($value)
    {
        echo "<button type='button'>$value</button>";
    }
    public static function p($isi,$rata='left')
    {
        echo "<p align='$rata'>$isi</p>".PHP_EOL;
    }
    public static function direct($link)
    {
        return header('location:'.ROOT.$link);
    }
        public static function winDirect($link)
    {
        return "<script>window.location='".ROOT.$link."</script>";
    }
    public static function post($name)
    {
        return $_POST[$name];
    }
    public static function get($name)
    {
        return $_GET[$name];
    }
    public static function line()
    {
    	echo "<hr>";
    }
}
?>