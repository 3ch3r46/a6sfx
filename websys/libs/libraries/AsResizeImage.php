<?php
class AsResizeImage
{
	private $prefix="thumbs_";
	private $height;
	private $width;
	private $path;
	private $nameFile;
	
	/**
	 * To resize image
	 * @param array $options
	 */
	function __construct($options=array()) {
		foreach($options as $key=>$value)
		{
			$this->{$key}($value);
		}
	}
	
	public function prefix($prefix)
	{
		$this->prefix=$prefix;
	}
	
	public function width($width)
	{
		$this->width=$width;
	}
	
	public function pathThumbs($path)
	{
		$this->path=$path;
	}
	
	public function height($height)
	{
		$this->height=$height;
	}
	
	public function resize($file)
	{
		$pos=strrpos($file, '.');
		$extention=substr($file, $pos+1,$pos+10);

		if(isset($this->path))
		{
			$path=$this->path;
		}else{
			$path=substr($file, strrpos($file, '/')-10000,strrpos($file, '/')+1);
		}
		$filename=substr($file, strrpos($file, '/')+1,strrpos($file, '/')+255);
		$file_name=$this->prefix.$filename;
		
		if($extention=="jpg" or $extention=="jpeg")
		{
			$images=imagecreatefromjpeg($file);
		}elseif($extention=="png")
		{
			$images=imagecreatefrompng($file);
		}elseif($extention=="gif")
		{
			$images=imagecreatefromgif($file);
		}elseif($extention=="wbmp"){
			$images=imagecreatefromwbmp($file);
		}elseif($extention=="xbm"){
			$images=imagecreatefromxbm($file);
		}
		else{
			throw new AsErrorException(405, "Type file with extension ".$extention." not allowed to resize images.<br>Allowed type file: jpg,jpeg,png and gif.");
			return false;
		}
		
		$image_width_original=imagesx($images);
		$image_height_original=imagesy($images);
		
		if(isset($this->width) and isset($this->height))
		{
			$thumbs_width=$this->width;
			$thumbs_height=$this->height;
		}elseif(isset($this->width) and !isset($this->height))
		{
			$thumbs_width=$this->width;
			$thumbs_height=($this->width/$image_width_original) * $image_height_original;
		}elseif(isset($this->height) and !isset($this->width))
		{
			$thumbs_height=$this->height;
			$thumbs_width=($this->height/$image_height_original) * $image_width_original;
		}else{
			$thumbs_width=100;
			$thumbs_height=($thumbs_width/$image_width_original) * $image_height_original;
		}
		
		$image_thumbs=imagecreatetruecolor($thumbs_width, $thumbs_height);
		imagecopyresampled($image_thumbs, $file, 0, 0, 0, 0, $thumbs_width, $thumbs_height, $image_width_original, $image_height_original);
		
		if($extention=="jpg" or $extention=="jpeg")
		{
			imagejpeg($image_thumbs,$path.$file_name);
		}elseif($extention=="png")
		{
			imagepng($image_thumbs,$path.$file_name);
		}elseif($extention=="gif")
		{
			imagegif($image_thumbs,$path.$file_name);
		}elseif($extention=="wbmp")
		{
			imagewbmp($image_thumbs,$path.$file_name);
		}elseif($extention=="xbm")
		{
			imagexbm($image_thumbs,$path.$file_name);
		}
		
		$this->nameFile=$file_name;
		imagedestroy($images);
		imagedestroy($image_thumbs);
	}
	
	public function getResizeFileName()
	{
		return $this->nameFile;
	}
}