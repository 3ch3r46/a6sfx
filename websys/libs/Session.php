<?php
class Session {
    
    public static function init()
    {
        @session_start();
    }
    public static function set($data)
    {
        if(is_array($data))
        {
            foreach($data as $key=>$value){
                $_SESSION[$key]=$value;
            }
        }
    }
    public static function get($data)
    {
    	@session_start();
        if(isset($data))
        return @$_SESSION[$data];
    }
    public static function destroy()
    {
        session_destroy();
    }
    public static function uns($ses)
    {
    	unset($_SESSION[$ses]);
    }
}
?>
