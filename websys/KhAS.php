<?php
/*
 * Defining the root directory
*/
list($root)=split('media.php',$_SERVER['PHP_SELF']);
define('ROOT',$root);
define('SYSTEM',$root.'websys/');
/*
 * ============================================
*/

/*
 * Require the web booting file
*/
require dirname(__FILE__).'/config/config.php';
require dirname(__FILE__).'/libs/AsErrorException.php';
require dirname(__FILE__).'/libs/boot.php';
require dirname(__FILE__).'/libs/controller.php';
require dirname(__FILE__).'/libs/view.php';
require dirname(__FILE__).'/libs/model.php';
require dirname(__FILE__).'/libs/APPS.php';
require dirname(__FILE__).'/libs/Auth.php';
require dirname(__FILE__).'/libs/Database.php';
require dirname(__FILE__).'/libs/Session.php';
/*
 * ============================================
*/

/*
 * Require the Widgets website

require dirname(__FILE__).'/libs/widget/AsActiveForm.php';
require dirname(__FILE__).'/libs/widget/AsForm.php';
require dirname(__FILE__).'/libs/widget/AsHtml.php';
require dirname(__FILE__).'/libs/widget/AsMenu.php';
require dirname(__FILE__).'/libs/widget/AsDetailView.php';
require dirname(__FILE__).'/libs/widget/AsCustomDetailView.php';
require dirname(__FILE__).'/libs/widget/AsGridView.php';
require dirname(__FILE__).'/libs/widget/AsListView.php';
require dirname(__FILE__).'/libs/widget/AsButton.php';
require dirname(__FILE__).'/libs/widget/AsSplitButton.php';
require dirname(__FILE__).'/libs/widget/AsHeroUnit.php';
require dirname(__FILE__).'/libs/widget/AsCarousel.php';
require dirname(__FILE__).'/libs/widget/AsModal.php';
require dirname(__FILE__).'/libs/widget/AsBreadcrumbs.php';
require dirname(__FILE__).'/libs/widget/AsNavbar.php';
require dirname(__FILE__).'/libs/widget/AsHeader.php';

/*
 * ========================================================
*/

/*
 * auto Require all class file
*/
$classesDir=array(
		dirname(__FILE__).'/libs/',
		dirname(__FILE__).'/libs/widget/',
		dirname(__FILE__).'/models/',
		dirname(__FILE__).'/libs/libraries/',
		);
function __autoload($class)
{
	global $classesDir;
	foreach($classesDir as $directory)
	{
		if(file_exists($directory.$class.'.php'))
		{
			require $directory.$class.'.php';
			return;
		}
	}
}
/*
 * ========================================================
*/

class KhAS
{
	public static $_app;
	
	public function __construct()
	{
		self::$_app=new APPS();
	}
	
	public static function app()
	{
		return self::$_app;
	}
	
	public static function run()
	{
		return new Boot();
	}
}