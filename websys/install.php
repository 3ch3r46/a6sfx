<?php if(file_exists('../websys/config/config.php')):?>
<?php header('location:../');?>
<?php else:?>
<link href="css/style.css" rel="stylesheet">
<style>
*{
	font-family:arial;
}
</style>
<link href='http://localhost/asfx/public/images/favicon.png' rel='shortcut icon' />
<meta name='viewport' content='width=device-width' />
<title>AS Framework Installation</title>
<h1>AS Framework Installation</h1>
<?php
if(@$_POST['install'])
{
	$webname=$_POST['webname'];
	$header=$_POST['header'];
	$slogan=$_POST['slogan'];
	$folder=$_POST['folder'];
	$dbname=$_POST['dbname'];
	$hostname=$_POST['hostname'];
	$dbuser=$_POST['dbuser'];
	$dbpass=$_POST['dbpass'];
	$gasActive=$_POST['gasActive'];
	$gasPass=$_POST['gasPass'];
	$confUser=$_POST['confUser'];
	$confPass=$_POST['confPass'];
	if($webname==null) $error[]="* Field Website Name are required.";
	if($header==null) $error[]="* Field Header are required.";
	if($dbname==null) $error[]="* Field Database Name are required.";
	if($hostname==null) $error[]="* Field Host Name are required.";
	if($gasPass==null) $error[]="* Field GAS Password are required.";
	if($confUser==null) $error[]="* Field User Configuration are required.";
	if($gasPass==null) $error[]="* Field Password GAS are required.";
	else $gasPass=md5($gasPass);
	if($confPass==null) $error[]="* Field Password Configuration are required.";
	else $confPass=md5($confPass);
	
	if(isset($error)){
		echo "<div style='background:#D84A38;padding:10px;color:#fff' align='left'><b>ERROR :</b><br>".implode("<br>", $error)."</div>";
	}else{
		$file=fopen('../websys/config/config.php', 'w');
		$fdata="<?php".PHP_EOL;
		$fdata.="/*
		*	this is a configuration of this framework
		*/".PHP_EOL;
		$fdata.="define('FolderName','".$folder."');".PHP_EOL;
		$fdata.="define('URL','http://'.\$_SERVER['HTTP_HOST'].FolderName.'/');//delete the '/asfx/' if this framework in online server".PHP_EOL;
		$fdata.="define('WebName','".$webname."');".PHP_EOL;
		$fdata.="define('Header','".$header."');".PHP_EOL;
		$fdata.="define('Slogan','".$slogan."');".PHP_EOL;
		$fdata.="define('Footer','Copyright &copy; '.date(\"Y\"));".PHP_EOL;
		$fdata.="/*
		 * This configuration for config a database connection
		 */".PHP_EOL;
		$fdata.="define('db','".$dbname."'); // your database name".PHP_EOL;
		$fdata.="define('host','".$hostname."'); // your hostname".PHP_EOL;
		$fdata.="define('user','".$dbuser."'); // your username of database".PHP_EOL;
		$fdata.="define('pass','".$dbpass."'); // your password of database".PHP_EOL;
		$fdata.="/*
		 * end of database configuration
		 */".PHP_EOL.PHP_EOL;
		
		$fdata.="/*
		 * This is a feature of GAS
		 * GAS is Generator Anam Script
		 * This feature to use for creating a script for this website.
		 * For create a CRUD : Create Read Update Delete, Form, And Anymore.
		 */".PHP_EOL;
		$fdata.="define('GAS','".$gasActive."');//set TRUE to active this feature or set FALSE to deactive".PHP_EOL;
		$fdata.="define('PassGAS','".$gasPass."');//password for login to GAS - Generate Anam Script".PHP_EOL;
		$fdata.="/*
		 * end of this feature
		 */".PHP_EOL;
		
		$fdata.="define('confUser','".$confUser."');//User for login in this configuration".PHP_EOL;
		$fdata.="define('confPass','".$confPass."');//password for login in this configuration".PHP_EOL;
		$fdata.="?>";
		
		fwrite($file,$fdata);
		fclose($file);
		if(copy('install.php','../websys/install.php')) unlink('install.php');
		header('location:../');
	}
}
?>
<form method="post" action="">
<fieldset>
	<legend>Website Configuration</legend>
	<div class='input'><label for="webname">Website Name</label>
	<input type="text" name="webname" value="<?php echo @$_POST['webname']; ?>">
	example: AS Framework</div>
	<div class='input'><label for="header">Header</label>
	<input type="text" name="header" value="<?php echo @$_POST['header']; ?>">
	example: AS Framework</div>
	<div class='input'><label for="slogan">Slogan</label>
	<textarea name="slogan" cols="30" rows="3"><?php echo @$_POST['slogan']; ?></textarea>
	example: This is a slogan of this Framework</div>
	<div class='input'><label for="foldername">Folder Name</label>
	<input type="text" name="folder" value="<?php echo @$_POST['folder']; ?>">
	example: '/ASFramework' or null</div>
</fieldset>
<fieldset>
	<legend>Database Configuration</legend>
	<div class='input'><label for="dbname">Database Name</label>
	<input type="text" name="dbname" value="<?php echo @$_POST['dbname']; ?>">
	example: mydatabase</div>
	<div class='input'><label for="hostname">Host Name</label>
	<input type="text" name="hostname" value="<?php echo @$_POST['hostname']; ?>">
	example: localhost</div>
	<div class='input'><label for="dbuser">Database Username</label>
	<input type="text" name="dbuser" value="<?php echo @$_POST['dbuser']; ?>">
	example: root</div>
	<div class='input'><label for="dbpass">Database Password</label>
	<input type="text" name="dbpass" value="<?php echo @$_POST['dbpass']; ?>">
	example: root or null</div>
</fieldset>
<fieldset>
	<legend>GAS Configuration</legend>
	<div class='input'>GAS is a Generator Anam Script<br>
	This feature is a feature to generate a Controllers, Models, Views, CRUD(Create Read Update Delete), Forms
	</div>
	<div class='input'><label for="gasActive">Activate GAS</label>
	<select name="gasActive">
		<option value="TRUE">Active</option>
		<option value="FALSE">Deactive</option>
	</select>
	To active or deactive this feature
	</div>
	<div class='input'><label for="gasPass">GAS Password</label>
	<input type="password" name="gasPass" value="<?php echo @$_POST['gasPass']; ?>">
	Required for login to GAS or Gerenator Anam Script</div>
</fieldset>
<fieldset>
	<legend>Web Configuration Login</legend>
	<div class='input'><label for="confUser">Configuration User</label>
	<input type="text" name="confUser" value="<?php echo @$_POST['confUser']; ?>">
	</div>
	<div class='input'><label for="confPass">Configuration Password</label>
	<input type="password" name="confPass" value="<?php echo @$_POST['confPass']; ?>">
	</div>
</fieldset>
<fieldset>
	<legend>Install AS Framework</legend>
<input type="submit" value=" Click Here If You Want To Install AS Framework Now " id="btna" name="install">
</fieldset>
</form>
<?php endif;?>