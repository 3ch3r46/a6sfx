<?php
class showMenu {
	function __construct() {
		if(Auth::getIsLogin())
		{
		    $menus=array(
		                'Home'=>'site',
		                'Berita'=>'berita',
		    			'Books'=>'books',
		                'User'=>'user',
		                'Logout'=>'Dashboard/out'
		            );
		}else{
		    $menus=array(
		                'Home'=>'site',
		                'About'=>array('go'=>'site/about','url'=>'go'),
	                    'Contact Us'=>'site/contact',
		                'login'=>'login',
		            );
		}
    $menu = new Menu($menus);
	}
}