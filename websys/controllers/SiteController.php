<?php

class SiteController extends Controller
{
	
	public function accessRoles()
	{
		return array(
				array('action'=>array('index','about','contact'),'user'=>'*'),
				array('action'=>array('login'),'user'=>'?'),
				array('action'=>array('logout'),'user'=>'@'),
		);
	}
	
	function __construct(){
		parent::__construct();
	}

	function actionIndex(){
		$this->view->render('site/index');
	}

	function actionAbout(){
		$this->view->render('site/about');
	}

	function actionContact(){
		$this->view->render('site/contact');
	}
	
	function actionLogin(){
		$model=new Login();
		 
		if($_POST['Login'])
		{
			$model->setAttributes($_POST['Login']);
			if($model->In())
				$this->direct(ROOT);
		}
		 
		$this->view->render('site/login',array('model'=>$model));
	}
	
	function actionLogout(){
		APPS::auth()->unsetAuth();
		$this->direct(array('site/login'));
	}
}