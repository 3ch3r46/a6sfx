<?php
class GasController extends Controller{

    function __construct() {
        parent::__construct();
        Session::init();
        if(GAS=="FALSE"){
            AsHtml::direct('');
        }
    }
    
    public function accessRoles()
    {
    	return array(
    			array('action'=>array('index','gasLogin','logout','form','crud','model','genModel','genForm','generate'),'user'=>'*')
    			);
    }
    
    function actionIndex()
    {
        if(Session::get('gaslog')=='TRUE')
        {
        	$this->menu();
        }else{
        	$this->login();
        }
    }
    
    function login()
    {
    	$this->rend('index');
    }
    
    function menu()
    {
    	$this->errorLog();
    	$this->rend('menu');
    }
    
    function actionGasLogin()
    {
        Session::init();
        if(md5(AsHtml::post('password'))==PassGAS)
        {
            Session::set(array('gaslog'=>'TRUE'));
        }
        AsHtml::direct('gas');
    }
    
    function actionLogout()
    {
        Session::init();
        Session::uns('gaslog');
        AsHtml::direct('gas');
    }
    
    function actionGenerate()
    {
	    $this->errorLog();
        require 'websys/generator/generate/generate.php';
    }
    
    function actionCrud()
    {
	    $this->errorLog();
		$this->rend('crud');
    }
    function actionModel()
    {
	    $this->errorLog();
    	$this->rend('model');
    }
    function actionGenModel()
    {
	    $this->errorLog();
	    $clas=strtolower(@$_GET['class']);
		if($clas=="gas"){
			AsHtml::script(array(
				"alert('Sorry, Model Name is not allowed!!!');self.history.back()"
		    ));
		}else{
	        require 'websys/generator/generate/gModel.php';
		}
    }
    
    function rend($ren)
    {
    	require 'websys/generator/header.php';
        require 'websys/generator/views/'.$ren.'.php';
        require 'websys/generator/footer.php';
    }
    
    function actionForm()
    {
    	$this->errorLog();
    	$this->rend('form');
    }
    
    function actionGenForm()
    {
    	$this->errorLog();
    	require 'websys/generator/generate/gForm.php';
    }
    function errorLog()
    {
	    session::init();
	    if(Session::get('gaslog')!='TRUE')
        {
        	$this->login();
        	exit;
        }
    }
}
?>