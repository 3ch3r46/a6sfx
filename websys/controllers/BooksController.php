<?php
class BooksController extends Controller{

	private $_offsetIndex=8; // this variable is a offset of view data in index page
	private $_offsetManage=10; // this variable is a offset of view data in management page

	/**
	 * run this class and class controller
	 */
	public function __construct(){
		parent::__construct(); // for run a controller class
	}
	
	/**
	 * Specifies for access control roles
	 * @return array access control roles
	 */
	public function accessRoles()
	{
		return array(
				array(
					'action'=>array('index','detail'),
					'user'=>'*', //allow for all user and guest
					),
				array(
					'action'=>array('create','update'),
					'user'=>'@', //allow for all user
					),
				array(
					'action'=>array('manage','delete'),
					'user'=>'#', //allow for admin
					),
			);
	}
	
	/**
	 * Displays data from model
	 * @param integer $id and the ID of the model to be displayed
	 */
	public function actionIndex($id=1){
		$model=new Books($id);
		$model->offset($this->_offsetIndex);
		
		$this->view->render('books/index',array(
					'model'=>$model,
				));
	}
	
	/**
	 * Displays data from model
	 * This action to control of Detail, Update, Delete the data
	 * @param integer $id and the ID of the model to be displayed
	 */
	public function actionManage($id=1){
		$model=new Books($id);
		$model->offset($this->_offsetManage);
		
		$this->view->render('books/manage',array(
				'model'=>$model,
				)); // for load a views
	}
	
	/**
	 * Display a data from model get from id model
	 * @param integer $id and the ID of the model to be displayed
	 */
	public function actionDetail($id=null){
		$model=$this->loadModel($id);
		
		$this->view->render('books/detail',array(
				'model'=>$model,
				));
	}
	
	/**
	 * Create New Model
	 * If creation is successful, browser will be redirect
	 */
	public function actionCreate(){
		$model=new Books();
		
		if($_POST['Books'])
		{
			$model->setAttributes($_POST['Books']);
			if($model->save()){
				$this->direct(array('Books'));
			}
		}
		
		$this->view->render('books/create',array('model'=>$model));
	}
	
	/**
	 * Updating model
	 * If Updateting is successful, browser will be redirect
	 * @param integer $id and the ID of the model to be displayed data
	 */
	public function actionUpdate($id){
		$model=$this->loadModel($id);
		
		if($_POST['Books'])
		{
			$model->setAttributes($_POST['Books']);
			if($model->save()){
				$this->direct(array('books/manage'));
			}
		}
		
		$this->view->render('books/update',array(
				'model'=>$model,
				));
	}
	
	/**
	 * Delete model
	 * @param integer $id and the ID of the model to be delete data
	 */
	public function actionDelete($id){
		$model=$this->loadModel($id);
		$model->delete(); // for delete data in database
		$this->direct(array('books/manage')); // for redirect to page
	}
	
	/**
	 * loadModel is a load a model does not create new model
	 * load data in model by id
	 * @param integer $id and the ID of the model to be displayed
	 */
	public function loadModel($id)
	{
		$model=Books::model()->findByPk($id);
		return $model;
	}

}
