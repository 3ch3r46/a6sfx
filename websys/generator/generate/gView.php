<?php
$classAs=$_GET['class'];
$class=ucfirst($classAs);
$mod=$class;
$tbls=new $mod;
$tbl=$tbls->table;
$dir=strtolower($class);
if(!is_dir(__DIR__.'/../../views/'.$dir)){
mkdir(__DIR__.'/../../views/'.$dir);
}
chmod(__DIR__.'/../../views/'.$dir, 0777);
//chown(__DIR__.'/../../views/'.$dir, 5001);
mysql_connect(HOSTNAME,HOST_USERNAME,HOST_PASSWORD);
mysql_select_db(DB_NAME);

// create a views for create form
$form=fopen(__DIR__.'/../../views/'.$dir.'/_form.php','w');
chmod(__DIR__.'/../../views/'.$dir.'/_form.php', 0777);
$dform="<?php
\$form=\$this->beginWidget('AsActiveForm',array(
'type'=>'horizontal',
));
?>

	<?php \$model->errorSummary(); ?>
".PHP_EOL;
$result=mysql_query("desc $tbl");
while($as=mysql_fetch_array($result))
{
	$ad=explode('(',$as['Type']);
	$tipe=$ad[0];
		if(($tipe=="varchar" or $tipe=="char" or $tipe=="double") and $as['Extra']!="auto_increment"){
		@$bts=str_replace(')','',$ad[1]);
		$dform.="	<?php \$form->textField(\$model,'".$as['Field']."',array('htmlOptions'=>array('class'=>'span4'),'label'=>'".str_replace('_',' ',$as['Field'])."')); ?>".PHP_EOL.PHP_EOL;
		}elseif(($tipe=="int" or $tipe=="tinyint" or $tipe=="smallint" or $tipe=="mediumint" or $tipe=="bigint" or $tipe=="bit") and $as['Extra']!="auto_increment"){
		@$bts=str_replace(')','',$ad[1]);
		$dform.="	<?php \$form->numberField(\$model,'".$as['Field']."',array('htmlOptions'=>array('class'=>'span4'),'label'=>'".str_replace('_',' ',$as['Field'])."')); ?>".PHP_EOL.PHP_EOL;
		}elseif($tipe=="date"){
		$dform.="	<?php \$form->dateField(\$model,'".$as['Field']."',array('htmlOptions'=>array('class'=>'span4'),'label'=>'".str_replace('_',' ',$as['Field'])."')); ?>".PHP_EOL.PHP_EOL;
		}elseif($tipe=="datetime"){
		$dform.="	<?php \$form->dateTimeField(\$model,'".$as['Field']."',array('htmlOptions'=>array('class'=>'span4'),'label'=>'".str_replace('_',' ',$as['Field'])."')); ?>".PHP_EOL.PHP_EOL;
		}elseif($tipe=="time"){
		$dform.="	<?php \$form->timeField(\$model,'".$as['Field']."',array('htmlOptions'=>array('class'=>'span4'),'label'=>'".str_replace('_',' ',$as['Field'])."')); ?>".PHP_EOL.PHP_EOL;
		}elseif($tipe=="text" or $tipe=="longtext" or $tipe=="mediumtext" or $tipe=="tinytext"){
		$dform.="	<?php \$form->textArea(\$model,'".$as['Field']."',array('htmlOptions'=>array('class'=>'span4'),'label'=>'".str_replace('_',' ',$as['Field'])."')); ?>".PHP_EOL.PHP_EOL;
		}elseif($tipe=="enum"){
		@$bts=str_replace(')','',$ad[1]);
		$bts=str_replace("'",'',$bts);
		$bts=explode(',',$bts);
		$dform.="	<?php \$form->radioListOptions(\$model,'".$as['Field']."',array(";
		foreach($bts as $val){
		$dform.="'".$val."',";
		}
		$dform.="\t),array('label'=>'".str_replace('_',' ',$as['Field'])."')); ?>".PHP_EOL;
		}
		}
		$dform.="
<div class='form-actions'>
	<?php \$this->widget('AsButton',array(
		'action'=>'submit',
		'type'=>'primary',
		'label'=>\$model->isNewRecord?'C R E A T E':'S A V E',
		));?>
</div>
<?php \$this->endWidget(); ?>".PHP_EOL;
fwrite($form,$dform);
fclose($form);


// create a views for create form
$crt=fopen(__DIR__.'/../../views/'.$dir.'/create.php','w');
chmod(__DIR__.'/../../views/'.$dir.'/create.php', 0777);
$dcrt="<?php\nAsHtml::title('Create ".$class."'); ?>

<div class='left'>
<h2>Create $class</h2>
<?php \$this->renderPartial('_form',array('model'=>\$model)); ?>
</div>
<div class='right'>
<?php \$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List $class','icon'=>'list','url'=>array('$class')),
	    		array('label'=>'Management $class','icon'=>'edit','url'=>array('$class/manage')),
	    		),
		));
?>
</div>".PHP_EOL;
fwrite($crt,$dcrt);
fclose($crt);


// create a views for edit form
$edt=fopen(__DIR__.'/../../views/'.$dir.'/update.php','w');
chmod(__DIR__.'/../../views/'.$dir.'/update.php', 0777);
$dedt="<?php\nAsHtml::title('Update ".$class."'); ?>

<div class='left'>
<h2>Update $class</h2>
<?php \$this->renderPartial('_form',array('model'=>\$model)); ?>
</div>
<div class='right'>
<?php \$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List $class','icon'=>'list','url'=>array('$class')),
	    		array('label'=>'Management $class','icon'=>'edit','url'=>array('$class/manage')),
	    		array('label'=>'Detail $class','icon'=>'search','url'=>array('$class/detail','id'=>\$model->{\$model->pk})),
	    		array('label'=>'Remove $class','icon'=>'remove','url'=>array('$class/delete','id'=>\$model->{\$model->pk}),'class'=>'del'),
	    		),
		));
?>
</div>".PHP_EOL;
$dedt.="
<?php
\$this->loadJScript('google.min.js');
\$this->registerJScript(\"
	$(document).ready(function(){
		$('.del').click(function(c){
			var c=confirm('Are you sure want to delete?');
			if(c == false) return false;
		});
	});
	\");".PHP_EOL;

$dedt.="?>";
fwrite($edt,$dedt);
fclose($edt);

// create a views for list
$lst=fopen(__DIR__.'/../../views/'.$dir.'/index.php','w');
chmod(__DIR__.'/../../views/'.$dir.'/index.php', 0777);
$dlst="<?php\nAsHtml::title('".$class."');".PHP_EOL;
$dlst.="AsHtml::startDiv(array('class'=>'left'));".PHP_EOL;
$dlst.="AsHtml::h(2,'".$class."');".PHP_EOL;
$dlst.="\$this->widget('AsListView',\$model, '_view');".PHP_EOL;
$dlst.="AsHtml::endDiv();".PHP_EOL;
$dlst.="AsHtml::startDiv(array('class'=>'right'));\n
\$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'Management $class','icon'=>'edit','url'=>array('$class/manage')),
	    		),
		));
/*
 *	in this if you want to add a new feature
 */
AsHtml::endDiv();".PHP_EOL;
$dlst.="?>";
fwrite($lst,$dlst);
fclose($lst);

// As List View
$viewd=fopen(__DIR__.'/../../views/'.$dir.'/_view.php','w');
chmod(__DIR__.'/../../views/'.$dir.'/_view.php', 0777);
$dviewd="
<div class=\"AsListView\">
<?php".PHP_EOL;
$result=mysql_query("desc $tbl");
while($as=mysql_fetch_array($result))
{
    if($as['Key']=="PRI"){
        $dviewd.="echo '<b>".str_replace('_',' ',$as['Field'])."</b> : '.AsHtml::link(APPS::createUrl(array('".$class."/detail','id'=>$".$as['Field'].")),'#'.$".$as['Field'].").'<br>';".PHP_EOL;
    }else{
    $dviewd.="echo '<b>".str_replace('_',' ',$as['Field'])."</b> : '.$".$as['Field'].".'<br>';".PHP_EOL;
    }
}
$dviewd.="?>
</div>";
fwrite($viewd,$dviewd);
fclose($viewd);

// create a view for manage
$mng=fopen(__DIR__.'/../../views/'.$dir.'/manage.php','w');
chmod(__DIR__.'/../../views/'.$dir.'/manage.php', 0777);
$dmng="<?php
AsHtml::title('Management ".$class."');
?>".PHP_EOL;
$dmng.="<div class='left'>
<?php".PHP_EOL;
$dmng.="AsHtml::h(2,'Management ".$class."');
\$this->widget('AsGridView',\$model,array(
		'columns'=>array(".PHP_EOL;
$result=mysql_query("desc $tbl");
while($as=  mysql_fetch_array($result))
{
	$dmng.="\t\t\t'".$as['Field']."',".PHP_EOL;
}
$dmng.="
				),
		'buttonAction'=>true,
		'navigation'=>array(
				'urlAction'=>'manage',
				),
		'searching'=>array(
				'searchField'=>array(".PHP_EOL;
$result=mysql_query("desc $tbl");
while($af=mysql_fetch_array($result))
{
    if($af['Key']!="PRI")
    {
        $dmng.="\t\t\t\t\t\t'".ucfirst(str_replace('_',' ',$af['Field']))."'=>'".$af['Field']."',".PHP_EOL;
    }
}
$dmng.="					),
				)
		));
?>
</div>
<div class='right'>
<?php".PHP_EOL;
$dmng.="
\$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List $class','icon'=>'list','url'=>array('$class')),
	    		array('label'=>'Create $class','icon'=>'plus','url'=>array('$class/create')),
	    		),
		));
/*
 *	in this if you want to add a new feature
 */
?>".PHP_EOL;
$dmng.="</div>
<?php".PHP_EOL;
$dmng.="\$this->loadJScript('google.min.js');".PHP_EOL;

$dmng.="?>".PHP_EOL;
fwrite($mng,$dmng);
fclose($mng);

$dtl=fopen(__DIR__.'/../../views/'.$dir.'/detail.php','w');
chmod(__DIR__.'/../../views/'.$dir.'/detail.php', 0777);
$ddtl="<?php\nAsHtml::title('Detail ".$class."');".PHP_EOL;
$ddtl.="AsHtml::startDiv(array('class'=>'left'));".PHP_EOL;
$ddtl.="\$this->widget('AsDetailView',\$model,array(";
$result=mysql_query("desc $tbl");
while($as=  mysql_fetch_array($result))
{
	if($as['Key']=="PRI")
		$ddtl.="
		'heading'=>'".$as['Field']."',";
}
$ddtl.="
		'items'=>array(";
$result=mysql_query("desc $tbl");
while($as=  mysql_fetch_array($result))
{
if($as['Key']!="PRI")
$ddtl.="
			array('label'=>'".ucfirst($as['Field'])."','name'=>'".$as['Field']."'),";
}
$ddtl.="
		),
));".PHP_EOL;
$ddtl.="AsHtml::endDiv();".PHP_EOL;
$ddtl.="AsHtml::startDiv(array('class'=>'right'));".PHP_EOL;
$ddtl.="
\$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List $class','icon'=>'list','url'=>array('$class')),
	    		array('label'=>'Management $class','icon'=>'edit','url'=>array('$class/manage')),
	    		array('label'=>'Update $class','icon'=>'pencil','url'=>array('$class/update','id'=>\$model->{\$model->pk})),
	    		array('label'=>'Remove $class','icon'=>'remove','url'=>array('$class/delete','id'=>\$model->{\$model->pk}),'class'=>'del'),
	    		),
		));
/*
 *	in this if you want to add a new feature
 */
AsHtml::endDiv();
\$this->loadJScript('google.min.js');
\$this->registerJScript(\"
		$(document).ready(function(){
			$('.del').click(function(c){
				var c=confirm('Are you sure want to delete?');
				if(c == false) return false;
			});
		});
\");".PHP_EOL;
$ddtl.="?>".PHP_EOL;
fwrite($dtl,$ddtl);
fclose($dtl);
?>