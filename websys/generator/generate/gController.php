<?php
$class=ucfirst($_GET['class']);
$names=strtolower($class);
$cf=fopen(__DIR__.'/../../controllers/'.$class.'Controller.php','w') or die('can\'t create a controller');
$data="<?php".PHP_EOL;
$data.="class ".$class."Controller extends Controller{".PHP_EOL;

$data.="
	private \$_offsetIndex=8; // this variable is a offset of view data in index page
	private \$_offsetManage=10; // this variable is a offset of view data in management page

	/**
	 * run this class and class controller
	 */
	public function __construct(){
		parent::__construct(); // for run a controller class
	}
	
	/**
	 * Specifies for access control roles
	 * @return array access control roles
	 */
	public function accessRoles()
	{
		return array(
				array(
					'action'=>array('index','detail'),
					'user'=>'*', //allow for all user and guest
					),
				array(
					'action'=>array('create','update'),
					'user'=>'@', //allow for all user
					),
				array(
					'action'=>array('manage','delete'),
					'user'=>'#', //allow for admin
					),
			);
	}
	
	/**
	 * Displays data from model
	 * @param integer \$id and the ID of the model to be displayed
	 */
	public function actionIndex(\$id=1){
		\$model=new $class(\$id);
		\$model->offset(\$this->_offsetIndex);
		
		\$this->view->render('".$names."/index',array(
					'model'=>\$model,
				));
	}
	
	/**
	 * Displays data from model
	 * This action to control of Detail, Update, Delete the data
	 * @param integer \$id and the ID of the model to be displayed
	 */
	public function actionManage(\$id=1){
		\$model=new $class(\$id);
		\$model->offset(\$this->_offsetManage);
		
		\$this->view->render('".$names."/manage',array(
				'model'=>\$model,
				)); // for load a views
	}
	
	/**
	 * Display a data from model get from id model
	 * @param integer \$id and the ID of the model to be displayed
	 */
	public function actionDetail(\$id=null){
		\$model=\$this->loadModel(\$id);
		
		\$this->view->render('".$names."/detail',array(
				'model'=>\$model,
				));
	}
	
	/**
	 * Create New Model
	 * If creation is successful, browser will be redirect
	 */
	public function actionCreate(){
		\$model=new $class();
		
		if(\$_POST['$class'])
		{
			\$model->setAttributes(\$_POST['$class']);
			if(\$model->save()){
				\$this->direct(array('$class'));
			}
		}
		
		\$this->view->render('".$names."/create',array('model'=>\$model));
	}
	
	/**
	 * Updating model
	 * If Updateting is successful, browser will be redirect
	 * @param integer \$id and the ID of the model to be displayed data
	 */
	public function actionUpdate(\$id){
		\$model=\$this->loadModel(\$id);
		
		if(\$_POST['$class'])
		{
			\$model->setAttributes(\$_POST['$class']);
			if(\$model->save()){
				\$this->direct(array('$names/manage'));
			}
		}
		
		\$this->view->render('".$names."/update',array(
				'model'=>\$model,
				));
	}
	
	/**
	 * Delete model
	 * @param integer \$id and the ID of the model to be delete data
	 */
	public function actionDelete(\$id){
		\$model=\$this->loadModel(\$id);
		\$model->delete(); // for delete data in database
		\$this->direct(array('".$names."/manage')); // for redirect to page
	}
	
	/**
	 * loadModel is a load a model does not create new model
	 * load data in model by id
	 * @param integer \$id and the ID of the model to be displayed
	 */
	public function loadModel(\$id)
	{
		\$model=$class::model()->findByPk(\$id);
		\$model->single();
		return \$model;
	}
".PHP_EOL;

$data.="}".PHP_EOL;

fwrite($cf,$data);
fclose($cf);
?>
