<?php
AsHtml::h(2,'CRUD Generator');
$fr=new AsForm(array('method'=>'get','action'=>ROOT.'gas/generate'));
$fr->textField('class',array(
	'htmlOptions'=>array(
    'placeholder'=>'Class Name',
    'required'=>'required'
		),
	'label'=>'Model Name',
));
$fr->button('Generate Script');
echo AsHtml::link(ROOT.'gas', 'Main Menu','button');
$fr->endForm();
AsHtml::h(4,'Create Read Update Delete');
AsHtml::line();
AsHtml::h(4,'To use this feature follow step by step');
AsHtml::p('<b>first</b> : Please create a model in model generator if you don\'t have a model. if you have a model, please next step.');
AsHtml::p('<b>second</b> : you input model name that has been created the before.');
AsHtml::p('<b>third</b> : you click generate button to generate a CRUD if not your click main menu to cancel generate a CRUD.');
?>
