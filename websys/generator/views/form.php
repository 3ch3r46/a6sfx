<?php
AsHtml::h(2,'Form Generator');
$fr=new AsForm(array('method'=>'get','action'=>ROOT.'gas/genForm'));
$fr->textField('class',array(
	'htmlOptions'=>array(
    'placeholder'=>'Model Name',
    'required'=>'required'
			),
	'label'=>'Model Name',
));
$fr->textField('fname',array(
	'htmlOptions'=>array(
    'placeholder'=>'Form Name',
    'required'=>'required'
			),
	'label'=>'Form Name',
));
$fr->button('Generate Script');
echo AsHtml::link(ROOT.'gas', 'Main Menu','button');
$fr->endForm();
AsHtml::line();
AsHtml::h(4,'To use this feature follow step by step');
AsHtml::p('<b>First</b> : insert model name in field model name that ready filled. If model name does not exist then form fail to generate.');
AsHtml::p('<b>Second</b> : input Form name in this field is a form name if not ready in this website. if Form name already exists in this website then this form fail to generate.');
AsHtml::p('<b>Third</b> : click generate script button to generate this form or click main menu to back to main menu.');
?>