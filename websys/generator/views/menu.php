<?php
APPS::widget('AsMenu',array(
	'type'=>'tabs',
	'stacked'=>true,
	'items'=>array(
		array('label'=>'CRUD Generator','url'=>array('gas/crud')),
		array('label'=>'FORM Generator','url'=>array('gas/form')),
		array('label'=>'MODEL Generator','url'=>array('gas/model')),
		array('label'=>'Logout GAS','url'=>array('gas/logout')),
	)
));
?>