<?php
AsHtml::h(1,'GAS Tutorial');
$menu = new VMenu(array(
	'CRUD Generator Tutorial'=>'gas/tutorial#crud',
	'MODEL Generator Tutorial'=>'gas/tutorial#model',
	'FORM Generator Tutorial'=>'gas/tutorial#form',
	'Back To Main Menu'=>'gas'
),'Tutorial Menu');
AsHtml::line();
AsHtml::h(2,'CRUD Tutorial',array('id'=>'crud'));
AsHtml::line();
AsHtml::h(3,'CRUD - Create Read Update Delete');
AsHtml::line();
AsHtml::h(4,'Definition CRUD');
AsHtml::p('<b>Create</b> : this is a feature in CRUD to insert data or add a data to database.');
AsHtml::p('<b>Read</b> : this is a feature in CRUD to Read data or listing data from database.');
AsHtml::p('<b>Update</b> : this is a feature in CRUD to Update data or change a data in database.');
AsHtml::p('<b>Delete</b> : this is a feature in CRUD to delete a data in database.');
AsHtml::line();
AsHtml::h(4,'To use this feature follow step by step');
AsHtml::p('<b>first</b> : Please create a model in model generator if you don\'t have a model. if you have a model, please next step.');
AsHtml::p('<b>second</b> : you input model name that has been created the before.');
AsHtml::p('<b>third</b> : you click generate button to generate a CRUD if not your click main menu to cancel generate a CRUD.');
AsHtml::line();
AsHtml::line();
AsHtml::h(2,'MODEL Tutorial',array('id'=>'model'));
AsHtml::p('Model is a feature in this framework include a function function about database. Connection to database. table data and anymore.');
AsHtml::line();
AsHtml::h(4,'To use this feature follow step by step');
AsHtml::p('<b>First</b> : insert model name in field model name that ready filled.');
AsHtml::p('<b>Second</b> : input table name in this field is a table ready in database. If the table name not exists then the model fail to generate.');
AsHtml::p('<b>Third</b> : click generate script button to generate this model or click main menu to back to main menu.');
AsHtml::line();
AsHtml::line();
AsHtml::h(2,'FORM Tutorial',array('id'=>'form'));
AsHtml::p('<b>First</b> : insert model name in field model name that ready filled. If model name does not exist then form fail to generate.');
AsHtml::p('<b>Second</b> : input Form name in this field is a form name if not ready in this website. if Form name already exists in this website then this form fail to generate.');
AsHtml::p('<b>Third</b> : click generate script button to generate this form or click main menu to back to main menu.');
