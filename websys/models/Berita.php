<?php
/**
 * This is the model class for table "posting"
 * 
 * The followings are the available columns in table 'posting':
 * @property int(10) $id
 * @property varchar(255) $judul
 * @property longtext $isi
 * @property varchar(50) $gambar
 * @property date $tanggal
 * @property varchar(50) $user
 */

class Berita extends Model
{
	public $id;
	public $judul;
	public $isi;
	public $gambar;
	public $tanggal;
	public $user;
	function __construct($id=null){
		parent::__construct($id); // for run a model class
		$this->pk='id';
		$this->{$this->pk}=$id;
		$this->model=__CLASS__;
		$this->table='posting';
		$this->setNewRecord();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Berita the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return array for validation rule for model attributes
	 */
	public function rules()
	{
		return array(
			array('judul,isi,gambar,tanggal,user','required'),
			);
	}
	
	/**
	 * @return array for table data field
	 */
	 
	public function field()
	{
		return array(
			'id'=>'Id',
			'judul'=>'Judul',
			'isi'=>'Isi',
			'gambar'=>'Gambar',
			'tanggal'=>'Tanggal',
			'user'=>'User',
		);
	}
}
