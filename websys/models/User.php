<?php
/**
 * This is the model class for table "user"
 * 
 * The followings are the available columns in table 'user':
 * @property int(10) $id
 * @property varchar(50) $first_name
 * @property varchar(50) $last_name
 * @property text $address
 * @property varchar(255) $email
 * @property varchar(50) $username
 * @property varchar(32) $password
 * @property varchar(255) $avatar
 * @property enum('admin','user','member') $level
 */

class User extends Model
{
	public $id;
	public $first_name;
	public $last_name;
	public $address;
	public $email;
	public $username;
	public $password;
	public $avatar;
	public $level;
	function __construct($id=null){
		parent::__construct($id); // for run a model class
		$this->pk='id';
		$this->{$this->pk}=$id;
		$this->model=__CLASS__;
		$this->table='user';
		$this->setNewRecord();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return array for validation rule for model attributes
	 */
	public function rules()
	{
		return array(
			array('first_name,last_name,address,email,username,password,level','required'),
			);
	}
	
	/**
	 * @return array for table data field
	 */
	 
	public function field()
	{
		return array(
			'id'=>'Id',
			'first_name'=>'First Name',
			'last_name'=>'Last Name',
			'address'=>'Address',
			'email'=>'Email',
			'username'=>'Username',
			'password'=>'Password',
			'avatar'=>'Avatar',
			'level'=>'Level',
		);
	}


}
