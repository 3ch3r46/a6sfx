<?php
/**
 * This is the model class for table "books"
 * 
 * The followings are the available columns in table 'books':
 * @property int(11) $id
 * @property varchar(150) $title
 * @property varchar(150) $author
 * @property text $description
 * @property tinyint(1) $on_sale
 */

class Books extends Model
{
	public $id;
	public $title;
	public $author;
	public $description;
	public $on_sale;
	function __construct($id=null){
		parent::__construct($id); // for run a model class
		$this->pk='id';
		$this->{$this->pk}=$id;
		$this->model=__CLASS__;
		$this->table='books';
		$this->setNewRecord();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Books the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return array for validation rule for model attributes
	 */
	public function rules()
	{
		return array(
			array('title,author,description,on_sale','required'),
			);
	}
	
	/**
	 * @return array for table data field
	 */
	 
	public function field()
	{
		return array(
			'id'=>'Id',
			'title'=>'Title',
			'author'=>'Author',
			'description'=>'Description',
			'on_sale'=>'On Sale',
		);
	}


}
