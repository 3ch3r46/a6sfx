<?php

class Login extends Model{
	
	public $username;
	public $password;
	
    function __construct() {
        parent::__construct();
        $this->model=__CLASS__;
        $this->table='user';
    }
    
   	public static function model($className=__CLASS__)
   	{
   		return parent::model($className);
   	}
   	
   	public function rules()
   	{
   		return array(
   				array('username,password','required'),
   				array('username','length','min'=>4,'max'=>8),
   				);
   	}
   	
   	public function field()
   	{
   		return array(
   				'username'=>'Username',
   				'password'=>'Password',
   				);
   	}
    
    function In()
    {
    	$this->validation();
    	/**
    	 * use this in comment if you want to connect this login to databases
    	 */
    	$db=$this->db->select('*','user',array(
            'username'=>$this->username,
            'and',
            'password'=>md5($this->password)
        ));
        $data=$db->fetch();
        $row=$db->rowCount();
        if($row==0)
        {
            return false;
        }else{
            $this->unsetAttributes();
            $this->setAttributes($data);
            APPS::auth()->setAuth(array(
            		'username'=>$this->username,
            		'level'=>$this->level,
            		));
            return true;
        }
        
    	/**
    	 * this login not use a database
    	 */
    	/**
    	$data=array(
    		'admin'=>array(
    			'username'=>'admin',
    			'password'=>'admin',
    			'level'=>'admin',
    		),
    		'user'=>array(
    			'username'=>'user',
    			'password'=>'user',
    			'level'=>'user',
    		),
    	);
    	$user=$data[$this->username];
    	if($user!=null){
    		$pass=$user['password'];
    		if($pass==$this->password)
    		{
    			APPS::auth()->setAuth(array(
    					'username'=>$this->username,
    					'level'=>$this->level,
    				));
    			Session::init();
    			Session::set(array(
    				'login'=>true,
    				'username'=>$data[AsHtml::post('username')],
    				'level'=>$data[AsHtml::post('username')]['level']
    			));
    			AsHtml::direct('dashboard');
    		}else{
    			AsHtml::direct('login');
    		}
    	}else{
    			AsHtml::direct('login');
    	}*/
    }

}
?>
