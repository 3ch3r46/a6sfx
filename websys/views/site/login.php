<?php
AsHTml::title('Login');
$form=$this->beginWidget('AsActiveForm',array(
		'legend'=>'Login',
		'type'=>'horizontal',
		'id'=>'login',
		));
$model->errorSummary();
$form->textField($model,'username',array(
		'label'=>'Username',
    ));
$form->passwordField($model,'password',array(
		'label'=>'Password',
    ));
AsForm::activeCheckBoxInLineOptions($model, 'rememberMe', array('Remember Me'=>'rememberMe'));
?>
<div class='form-actions'>
<?php $this->widget('AsButton',array(
		'type'=>'primary',
		'label'=>'Login',
		'action'=>'submit',
		));?>
<?php $this->widget('AsButton',array(
		'type'=>'inverse',
		'label'=>'Create New Account',
		'url'=>array('user/create'),
		));?>
</div>
<?php $this->endWidget(); ?>