<?php AsHtml::title('Contact')?>
<?php
$this->beginWidget('AsForm',array(
		'legend'=>'Contact Us',
		'type'=>'horizontal'
		));
AsForm::textField(
		'name',
		array(
			'htmlOptions'=>array(
				'class'=>'span4',
				'placeholder'=>'Fill this field with your name',
				'required'=>'',
				'title'=>'fill'
					),
			'label'=>'Name :',
		)
	);
AsForm::textField(
			'name',
			array(
				'htmlOptions'=>array(
					'class'=>'span4',
					'placeholder'=>'Fill this field with your email',
					),
				'label'=>'Email :',
			)
	);
AsForm::textArea(
		'address',
		array(
			'htmlOptions'=>array(
				'class'=>'span4',
				'rows'=>1,
				'placeholder'=>'Fill this field with your address',
				),
			'label'=>'Address :',
		)
	);
AsForm::textArea(
			'message',
			array(
				'htmlOptions'=>array(
						'class'=>'span4',
						'cols'=>30,
						'rows'=>5,
						'placeholder'=>'Fill this field with your Message',
					),
				'label'=>'Messages :',
			)
	);
?>
<div class="form-actions">
<div class="btn-group">
<?php $this->widget('AsButton',array(
		'label'=>'Send Messages',
		'action'=>'submit',
		'type'=>'primary',
		));?>
<?php $this->widget('AsButton',array(
		'label'=>'Reset',
		'action'=>'reset',
		'type'=>'inverse',
		));?>
</div>
</div>