<?php AsHtml::title('About')?>
<div class="columns">
<div class="left"><h1>About</h1>
<?php $this->widget('AsCarousel',array(
                        'id'=>'NewCarousel',
                        'location'=>'',
                        'items'=>array(
                                        array('images'=>'coverFramework.jpg','label'=>'Framework Cover','content'=>'This framework is easy to use.'),
                                        array('images'=>'HTML5addin.jpg','label'=>'HTML 5 AddIn','content'=>'Using Feature Html 5.'),
                                        array('images'=>'css3addin.jpg','label'=>'CSS 3 AddIn','content'=>'Using CSS 3 for styling this design.'),
                                        array('images'=>'php5addin.jpg','label'=>'PHP 5 AddIn','content'=>'Using PHP 5 For server side scripting.'),
                                        array('images'=>'jQueryaddin.jpg','label'=>'jQuery AddIn','content'=>'Using JQuery Feature for animation in this site.'),
                                        array('images'=>'GASAddin.jpg','label'=>'GAS','content'=>'Generator Anam Script.'),
                                        )
                ));?>
                
<?php $this->widget('AsSplitButton',array(
                'label'=>'Split Button',
                'type'=>'danger',
                'items'=>array(
                                array('label'=>'satu','url'=>'#'),
                                array('label'=>'dua','url'=>'#'),
                                array('label'=>'tiga','url'=>'#'),
                                '---',
                                array('label'=>'empat','url'=>'#'),
                                array('label'=>'lima','url'=>'#'),
                                array('label'=>'enam','url'=>'#'),
                        )
                ));?>
</div>
<div class="right">
<?php APPS::widget('AsMenu',array(
        'type'=>'tabs',
        'stacked'=>true,
        'useFrame'=>true,
        'items'=>Array(
                        array('label'=>'Pro Language','type'=>'header'),
                        array('label'=>'HTML5','url'=>'#'),
                        array('label'=>'CSS3','url'=>'#'),
                        array('label'=>'PHP5','url'=>'#'),
                        array('label'=>'JQUERY','url'=>'#')
                        ),
));?>
<?php APPS::widget('AsMenu',array(
        'type'=>'pills',
        'stacked'=>true,
        'useFrame'=>true,
        'items'=>Array(
                        array('label'=>'GAS Feature','type'=>'header'),
                        array('label'=>'CRUD Generator','url'=>'#'),
                        array('label'=>'Model Generator','url'=>'#'),
                        array('label'=>'Form Generator','url'=>'#'),
                        ),
));?>
</div>
</div>