<?php
AsHtml::title('Update Books'); ?>

<div class='left'>
<h2>Update Books</h2>
<?php $this->renderPartial('_form',array('model'=>$model)); ?>
</div>
<div class='right'>
<?php $this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List Books','icon'=>'list','url'=>array('Books')),
	    		array('label'=>'Management Books','icon'=>'edit','url'=>array('Books/manage')),
	    		array('label'=>'Detail Books','icon'=>'search','url'=>array('Books/detail','id'=>$model->{$model->pk})),
	    		array('label'=>'Remove Books','icon'=>'remove','url'=>array('Books/delete','id'=>$model->{$model->pk}),'class'=>'del'),
	    		),
		));
?>
</div>

<?php
$this->loadJScript('google.min.js');
$this->registerJScript("
	$(document).ready(function(){
		$('.del').click(function(c){
			var c=confirm('Are you sure want to delete?');
			if(c == false) return false;
		});
	});
	");
?>