<?php
AsHtml::title('Management Books');
?>
<div class='left'>
<?php
AsHtml::h(2,'Management Books');
$this->widget('AsGridView',$model,array(
		'columns'=>array(
			'id',
			'title',
			'author',
			'description',
			'on_sale',

				),
		'buttonAction'=>true,
		'navigation'=>array(
				'urlAction'=>'manage',
				),
		'searching'=>array(
				'searchField'=>array(
						'Title'=>'title',
						'Author'=>'author',
						'Description'=>'description',
						'On sale'=>'on_sale',
					),
				)
		));
?>
</div>
<div class='right'>
<?php

$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List Books','icon'=>'list','url'=>array('Books')),
	    		array('label'=>'Create Books','icon'=>'plus','url'=>array('Books/create')),
	    		),
		));
/*
 *	in this if you want to add a new feature
 */
?>
</div>
<?php
$this->loadJScript('google.min.js');
?>
