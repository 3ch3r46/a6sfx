<?php
AsHtml::title('Create Books'); ?>

<div class='left'>
<h2>Create Books</h2>
<?php $this->renderPartial('_form',array('model'=>$model)); ?>
</div>
<div class='right'>
<?php $this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List Books','icon'=>'list','url'=>array('Books')),
	    		array('label'=>'Management Books','icon'=>'edit','url'=>array('Books/manage')),
	    		),
		));
?>
</div>
