<?php
AsHtml::title('Books');
AsHtml::startDiv(array('class'=>'left'));
AsHtml::h(2,'Books');
$this->widget('AsListView',$model, '_view');
AsHtml::endDiv();
AsHtml::startDiv(array('class'=>'right'));

$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'Management Books','icon'=>'edit','url'=>array('Books/manage')),
	    		),
		));
/*
 *	in this if you want to add a new feature
 */
AsHtml::endDiv();
?>