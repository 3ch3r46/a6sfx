<?php
$form=$this->beginWidget('AsActiveForm',array(
'type'=>'horizontal',
));
?>

	<?php $model->errorSummary(); ?>

	<?php $form->textField($model,'title',array('htmlOptions'=>array('class'=>'span4'),'label'=>'title')); ?>

	<?php $form->textField($model,'author',array('htmlOptions'=>array('class'=>'span4'),'label'=>'author')); ?>

	<?php $form->textArea($model,'description',array('htmlOptions'=>array('class'=>'span4'),'label'=>'description')); ?>

	<?php $form->numberField($model,'on_sale',array('htmlOptions'=>array('class'=>'span4'),'label'=>'on sale')); ?>


<div class='form-actions'>
	<?php $this->widget('AsButton',array(
		'action'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord?'C R E A T E':'S A V E',
		));?>
</div>
<?php $this->endWidget(); ?>
