<?php
AsHtml::title('Detail Books');
AsHtml::startDiv(array('class'=>'left'));
$this->widget('AsDetailView',$model,array(
		'heading'=>'id',
		'items'=>array(
			array('label'=>'Title','name'=>'title'),
			array('label'=>'Author','name'=>'author'),
			array('label'=>'Description','name'=>'description'),
			array('label'=>'On_sale','name'=>'on_sale'),
		),
));
AsHtml::endDiv();
AsHtml::startDiv(array('class'=>'right'));

$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List Books','icon'=>'list','url'=>array('Books')),
	    		array('label'=>'Management Books','icon'=>'edit','url'=>array('Books/manage')),
	    		array('label'=>'Update Books','icon'=>'pencil','url'=>array('Books/update','id'=>$model->{$model->pk})),
	    		array('label'=>'Remove Books','icon'=>'remove','url'=>array('Books/delete','id'=>$model->{$model->pk}),'class'=>'del'),
	    		),
		));
/*
 *	in this if you want to add a new feature
 */
AsHtml::endDiv();
$this->loadJScript('google.min.js');
$this->registerJScript("
		$(document).ready(function(){
			$('.del').click(function(c){
				var c=confirm('Are you sure want to delete?');
				if(c == false) return false;
			});
		});
");
?>
