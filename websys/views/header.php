<!DOCTYPE html>
<html>
	<head>
		<?php
		AsHtml::meta('viewport','width=device-width');
		AsHtml::style('style');
		$this->loadStyleSheet('bootstrap.css');
		$this->loadStyleSheet('bootstrap-responsive.css');
		$this->loadStyleSheet('jscrollpane.css');
		AsHtml::favicon('favicon.png');
		APPS::loadJScript('jquery-1.7.2.js');
		APPS::loadJScript('transition.js');
		?>
	</head>
<body>
	<?php $this->widget('AsNavbar',array(
		'brand'=>WebName,
		'type'=>'purple',
		'collapse'=>true,
		'items'=>array(
				array('label'=>'Home','url'=>ROOT,'icon'=>'home'),
				array('label'=>'Berita','icon'=>'globe','url'=>'#','visible'=>Auth::getIsLogin(),'items'=>array(
						array('label'=>'Berita','type'=>'header'),
						array('label'=>'list','icon'=>'list','url'=>array('berita')),
						array('label'=>'create','icon'=>'plus','url'=>array('berita/create')),
						array('label'=>'manage','icon'=>'edit','url'=>array('berita/manage')),
						)),
				array('label'=>'Books','icon'=>'book','url'=>array('books'),'visible'=>Auth::getIsLogin()),
				array('label'=>'User','icon'=>'user','url'=>array('user'),'visible'=>Auth::getIsLogin()),
				array('label'=>'About','icon'=>'user','url'=>array('site/about'),'visible'=>!Auth::getIsLogin()),
				array('label'=>'Contact','icon'=>'comment','url'=>array('site/contact'),'visible'=>!Auth::getIsLogin()),
				array('label'=>'Login','icon'=>'lock','url'=>array('site/login'),'visible'=>!Auth::getIsLogin()),
				array('label'=>'Logout','icon'=>'off','url'=>array('site/logout'),'visible'=>Auth::getIsLogin()),
				),
		));
	?>
<div class="wrapper">
		<div class="container" id="page">
			<?php $this->widget('AsBreadcrumbs');?>
			<div class="inner">
				<div class="content">