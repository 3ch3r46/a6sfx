<?php
$form=$this->beginWidget('AsActiveForm',array(
			'type'=>'horizontal',
		));

$form->textField($model,'judul',array(
        		'htmlOptions'=>array(
        			'class'=>'span4',
        			'placeholder'=>'judul',
       				'maxlength'=>255,
       				'required'=>'required'
       			),
       			'label'=>'judul'
        	));
$form->textArea($model,'isi',array(
        	'htmlOptions'=>array(
        		'class'=>'span4',
        		'rows'=>4,
        		'placeholder'=>'isi',
        		'required'=>'required'
        	),
        	'label'=>'isi'
        	));
$form->textField($model,'gambar',array(
        		'htmlOptions'=>array(
        			'class'=>'span4',
        			'placeholder'=>'gambar',
       				'maxlength'=>50,
       				'required'=>'required'
       			),
       			'label'=>'gambar'
        	));
$form->dateField($model,'tanggal',array(
        		'htmlOptions'=>array(
        			'class'=>'span4',
        			'placeholder'=>'tanggal',
       				'required'=>'required'
       			),
       			'label'=>'tanggal'
        	));
$form->textField($model,'user',array(
        		'htmlOptions'=>array(
        			'class'=>'span4',
        			'placeholder'=>'user',
       				'maxlength'=>50,
       				'required'=>'required'
       			),
       			'label'=>'user'
        	));

?>
<div class='form-actions'>
<?php $this->widget('AsButton',array(
			'action'=>'submit',
			'type'=>'primary',
			'label'=>'S A V E',
		));?>
</div>
<?php $this->endWidget();?>