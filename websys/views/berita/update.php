<?php
AsHtml::title('Update Berita'); ?>

<div class='left'>
<h2>Update Berita</h2>
<?php $this->renderPartial('_form',array('model'=>$model)); ?>
</div>
<div class='right'>
<?php $this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List Berita','icon'=>'list','url'=>array('Berita')),
	    		array('label'=>'Management Berita','icon'=>'edit','url'=>array('Berita/manage')),
	    		array('label'=>'Detail Berita','icon'=>'search','url'=>array('Berita/detail','id'=>$model->{$model->pk})),
	    		array('label'=>'Remove Berita','icon'=>'remove','url'=>array('Berita/delete','id'=>$model->{$model->pk}),'class'=>'del'),
	    		),
		));
?>
</div>

<?php
$this->loadJScript('google.min.js');
$this->registerJScript("
	$(document).ready(function(){
		$('.del').click(function(c){
			var c=confirm('Are you sure want to delete?');
			if(c == false) return false;
		});
	});
	");
?>