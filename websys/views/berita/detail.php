<?php
AsHtml::title('Detail Berita');
AsHtml::startDiv(array('class'=>'left'));
$this->widget('AsDetailView',$model,array(
		'heading'=>'id',
		'items'=>array(
			array('label'=>'Judul','name'=>'judul'),
			array('label'=>'Isi','name'=>'isi'),
			array('label'=>'Gambar','name'=>'gambar'),
			array('label'=>'Tanggal','name'=>'tanggal'),
			array('label'=>'User','name'=>'user'),
		),
));
AsHtml::endDiv();
AsHtml::startDiv(array('class'=>'right'));

$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List Berita','icon'=>'list','url'=>array('Berita')),
	    		array('label'=>'Management Berita','icon'=>'edit','url'=>array('Berita/manage')),
	    		array('label'=>'Update Berita','icon'=>'pencil','url'=>array('Berita/update','id'=>$model->{$model->pk})),
	    		array('label'=>'Remove Berita','icon'=>'remove','url'=>array('Berita/delete','id'=>$model->{$model->pk}),'class'=>'del'),
	    		),
		));
/*
 *	in this if you want to add a new feature
 */
AsHtml::endDiv();
$this->loadJScript('google.min.js');
$this->registerJScript("
		$(document).ready(function(){
			$('.del').click(function(c){
				var c=confirm('Are you sure want to delete?');
				if(c == false) return false;
			});
		});
");
?>
