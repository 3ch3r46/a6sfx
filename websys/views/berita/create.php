<?php
AsHtml::title('Create Berita'); ?>

<div class='left'>
<h2>Create Berita</h2>
<?php $this->renderPartial('_form',array('model'=>$model)); ?>
</div>
<div class='right'>
<?php $this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List Berita','icon'=>'list','url'=>array('Berita')),
	    		array('label'=>'Management Berita','icon'=>'edit','url'=>array('Berita/manage')),
	    		),
		));
?>
</div>
