<?php
AsHtml::title('Management Berita');
?>
<div class='left'>
<?php
AsHtml::h(2,'Management Berita');
$this->widget('AsGridView',$model,array(
		'columns'=>array(
			'id',
			'judul',
			'isi',
			'gambar',
			'tanggal',
			'user',

				),
		'buttonAction'=>true,
		'navigation'=>array(
				'urlAction'=>'manage',
				),
		'searching'=>array(
				'searchField'=>array(
						'Judul'=>'judul',
						'Isi'=>'isi',
						'Gambar'=>'gambar',
						'Tanggal'=>'tanggal',
						'User'=>'user',
					),
				)
		));
?>
</div>
<div class='right'>
<?php

$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List Berita','icon'=>'list','url'=>array('Berita')),
	    		array('label'=>'Create Berita','icon'=>'plus','url'=>array('Berita/create')),
	    		),
		));
/*
 *	in this if you want to add a new feature
 */
?>
</div>
<?php
$this->loadJScript('google.min.js');
?>
