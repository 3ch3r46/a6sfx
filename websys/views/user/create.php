<?php
AsHtml::title('Create User'); ?>

<div class='left'>
<h2>Create User</h2>
<?php $this->renderPartial('_form',array('model'=>$model)); ?>
</div>
<div class='right'>
<?php $this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List User','icon'=>'list','url'=>array('User')),
	    		array('label'=>'Management User','icon'=>'edit','url'=>array('User/manage')),
	    		),
		));
?>
</div>
