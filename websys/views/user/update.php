<?php
AsHtml::title('Update User'); ?>

<div class='left'>
<h2>Update User</h2>
<?php $this->renderPartial('_form',array('model'=>$model)); ?>
</div>
<div class='right'>
<?php $this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List User','icon'=>'list','url'=>array('User')),
	    		array('label'=>'Management User','icon'=>'edit','url'=>array('User/manage')),
	    		array('label'=>'Detail User','icon'=>'search','url'=>array('User/detail','id'=>$model->val_pk)),
	    		array('label'=>'Remove User','icon'=>'remove','url'=>array('User/delete','id'=>$model->val_pk),'class'=>'del'),
	    		),
		));
?>
</div>

<?php
$this->loadJScript('google.min.js');
$this->registerJScript("
	$(document).ready(function(){
		$('.del').click(function(c){
			var c=confirm('Are you sure want to delete?');
			if(c == false) return false;
		});
	});
	");
?>