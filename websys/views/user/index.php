<?php
AsHtml::title('User');
AsHtml::startDiv(array('class'=>'left'));
AsHtml::h(2,'User');
$this->widget('AsListView',$model, '_view');
AsHtml::endDiv();
AsHtml::startDiv(array('class'=>'right'));

$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'Management User','icon'=>'edit','url'=>array('User/manage')),
	    		),
		));
/*
 *	in this if you want to add a new feature
 */
AsHtml::endDiv();
?>