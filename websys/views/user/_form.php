<?php
$form=$this->beginWidget('AsActiveForm',array(
'type'=>'horizontal',
));
?>

	<?php $model->errorSummary(); ?>

	<?php $form->textField($model,'first_name',array('htmlOptions'=>array('class'=>'span4'),'label'=>'first name')); ?>

	<?php $form->textField($model,'last_name',array('htmlOptions'=>array('class'=>'span4'),'label'=>'last name')); ?>

	<?php $form->textArea($model,'address',array('htmlOptions'=>array('class'=>'span4'),'label'=>'address')); ?>

	<?php $form->textField($model,'email',array('htmlOptions'=>array('class'=>'span4'),'label'=>'email')); ?>

	<?php $form->textField($model,'username',array('htmlOptions'=>array('class'=>'span4'),'label'=>'username')); ?>

	<?php $form->textField($model,'password',array('htmlOptions'=>array('class'=>'span4'),'label'=>'password')); ?>

	<?php $form->textField($model,'avatar',array('htmlOptions'=>array('class'=>'span4'),'label'=>'avatar')); ?>

	<?php $form->radioListOptions($model,'level',array('admin','user','member',	),array('label'=>'level')); ?>

<div class='form-actions'>
	<?php $this->widget('AsButton',array(
		'action'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord?'C R E A T E':'S A V E',
		));?>
</div>
<?php $this->endWidget(); ?>
