<?php
AsHtml::title('Detail User');
AsHtml::startDiv(array('class'=>'left'));
$this->widget('AsDetailView',$model,array(
		'heading'=>'id',
		'items'=>array(
			array('label'=>'First_name','name'=>'first_name'),
			array('label'=>'Last_name','name'=>'last_name'),
			array('label'=>'Address','name'=>'address'),
			array('label'=>'Email','name'=>'email'),
			array('label'=>'Username','name'=>'username'),
			array('label'=>'Password','name'=>'password'),
			array('label'=>'Avatar','name'=>'avatar'),
			array('label'=>'Level','name'=>'level'),
		),
));
AsHtml::endDiv();
AsHtml::startDiv(array('class'=>'right'));

$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List User','icon'=>'list','url'=>array('User')),
	    		array('label'=>'Management User','icon'=>'edit','url'=>array('User/manage')),
	    		array('label'=>'Update User','icon'=>'pencil','url'=>array('User/update','id'=>$model->{$model->pk})),
	    		array('label'=>'Remove User','icon'=>'remove','url'=>array('User/delete','id'=>$model->{$model->pk}),'class'=>'del'),
	    		),
		));
/*
 *	in this if you want to add a new feature
 */
AsHtml::endDiv();
$this->loadJScript('google.min.js');
$this->registerJScript("
		$(document).ready(function(){
			$('.del').click(function(c){
				var c=confirm('Are you sure want to delete?');
				if(c == false) return false;
			});
		});
");
?>
