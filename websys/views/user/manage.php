<?php
AsHtml::title('Management User');
?>
<div class='left'>
<?php
AsHtml::h(2,'Management User');
$this->widget('AsGridView',$model,array(
		'columns'=>array(
			'id',
			'first_name',
			'last_name',
			'address',
			'email',
			'username',
			'password',
			'avatar',
			'level',

				),
		'buttonAction'=>true,
		'navigation'=>array(
				'urlAction'=>'manage',
				),
		'searching'=>array(
				'searchField'=>array(
						'First name'=>'first_name',
						'Last name'=>'last_name',
						'Address'=>'address',
						'Email'=>'email',
						'Username'=>'username',
						'Password'=>'password',
						'Avatar'=>'avatar',
						'Level'=>'level',
					),
				)
		));
?>
</div>
<div class='right'>
<?php

$this->widget('AsMenu',array(
			'type'=>'tabs',
			'stacked'=>true,
			'items'=>array(
	    		array('label'=>'List User','icon'=>'list','url'=>array('User')),
	    		array('label'=>'Create User','icon'=>'plus','url'=>array('User/create')),
	    		),
		));
/*
 *	in this if you want to add a new feature
 */
?>
</div>
<?php
$this->loadJScript('google.min.js');
?>
