<?php
/* uncomment here if you want to use footer content
AsHtml::startDiv(array('class'=>'footContent'));
AsHtml::startDiv(array('class'=>'footIsian'));
echo "here a foot content";
AsHtml::endDiv();
AsHtml::endDiv();
*/
?>
			</div><!-- inner -->
		</div><!-- content -->
	</div><!-- container -->
	<div class="footer">
		<div class="container">
			<div class="inner">
				<?php
				AsHtml::p(Footer." ".WebName,'center');
				AsHtml::p(Powered,'center');
				?>
			</div><!-- inner -->
		</div><!-- container -->
	</div><!-- footer -->
</div><!-- wrapper -->
<?php
$this->runJScript();
?>
</body>
</html>